package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Adapters.AsistenciaGeneralAdapter;
import com.cucinestudios.mastertoolsmaestros.Models.AsistenciaGeneralItem;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

/**
 * Created by jesusnieves on 19/5/17.
 */
public class AsistenciaGeneralActivity extends AppCompatActivity {

    FloatingActionButton crear;
    RecyclerView lista;
    FloatingActionButton guardar;
    RecyclerView.LayoutManager manager;
    Activity activity;
    ArrayList<AsistenciaGeneralItem> asistencia_generalItems;
    AsistenciaGeneralAdapter asistencia_generalAdapter;
    Config config;
    Realm realm;
    TrailingCircularDotsLoader loading;
    String id;
    int bimestre;
    SwipeRefreshLayout refreshLayout;
    FrameLayout play;
    JSONArray asistenciaObj;
    String idbimestre;
    FloatingActionMenu menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asistencia_general);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        play = (FrameLayout) toolbar.findViewById(R.id.asistencia_general_play);
        toolbar.setTitle("Asistencia General");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config = new Config();
        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.asistencia_general_refresh);
        menu = (FloatingActionMenu) findViewById(R.id.menu);
        crear = (FloatingActionButton) findViewById(R.id.asistencia_general_tomar);
        lista = (RecyclerView) findViewById(R.id.asistencia_general_lista);
        guardar = (FloatingActionButton) findViewById(R.id.asistencia_general_guardar);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
            bimestre = bundle.getInt("bimestre");
            Log.e("id",id);
            Log.e("bimestre",bimestre+"");
        }
        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu.close(true);
                submitTomar();
            }
        });
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu.close(true);
               submit();
            }
        });

        refreshLayout.setColorSchemeColors(ContextCompat.getColor(activity,R.color.azul_oscuro));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListaRefresh();
            }
        });

        lista.setHasFixedSize(true);
        manager = new LinearLayoutManager(activity);
        lista.setLayoutManager(manager);
        getLista();
    }

    public void getListaRefresh(){
        asistencia_generalItems.clear();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("grupo",id);
        params.put("bimestre",bimestre);
        String url = config.getUrl()+config.getAlumnos();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1) {
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray alumnos = data.getJSONArray("alumnos");
                        Log.e("al",alumnos.length()+"");
                        for (int i = 0; i<alumnos.length(); i++){
                            String name = alumnos.getJSONObject(i).getString("ApellidoPaterno")+" "+alumnos.getJSONObject(i).getString("ApellidoMaterno")+" "+alumnos.getJSONObject(i).getString("Nombre");
                            int tipo = alumnos.getJSONObject(i).getInt("Estado");
                            asistencia_generalItems.add(new AsistenciaGeneralItem(alumnos.getJSONObject(i).getString("IDAlumno"),name,tipo,alumnos.getJSONObject(i).getString("IDAlumno")));
                        }
                        asistencia_generalAdapter.notifyDataSetChanged();
                        refreshLayout.setRefreshing(false);
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }

    public void getLista(){
        asistencia_generalItems = new ArrayList<>();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("grupo",id);
        params.put("bimestre",bimestre);
        String url = config.getUrl()+config.getAlumnos();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1) {
                        idbimestre = respuesta.getString("IDBimestre");
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray alumnos = data.getJSONArray("alumnos");
                        Log.e("al",alumnos.length()+"");
                        for (int i = 0; i<alumnos.length(); i++){
                            String name = alumnos.getJSONObject(i).getString("ApellidoPaterno")+" "+alumnos.getJSONObject(i).getString("ApellidoMaterno")+" "+alumnos.getJSONObject(i).getString("Nombre");
                            int tipo = alumnos.getJSONObject(i).getInt("Estado");
                            asistencia_generalItems.add(new AsistenciaGeneralItem(alumnos.getJSONObject(i).getString("IDAlumno"),name,tipo,alumnos.getJSONObject(i).getString("IDAlumno")));
                        }
                        asistencia_generalAdapter = new AsistenciaGeneralAdapter(activity,asistencia_generalItems);
                        lista.setAdapter(asistencia_generalAdapter);
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }


    void submitTomar(){
        asistenciaObj = new JSONArray();
        for(int i = 0; i<asistencia_generalItems.size(); i++){
            JSONObject student1 = new JSONObject();
            try {
                student1.put("IDAlumno", asistencia_generalItems.get(i).getId());
                student1.put("Estado", 1);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            asistenciaObj.put(student1);
        }
        crearAsistencia();

    }

    void submit(){
        asistenciaObj = new JSONArray();
        for(int i = 0; i<asistencia_generalItems.size(); i++){
            Log.e("tipo",asistencia_generalItems.get(i).getTipo()+"");
            JSONObject student1 = new JSONObject();
            try {
                student1.put("IDAlumno", asistencia_generalItems.get(i).getId());
                student1.put("Estado", asistencia_generalItems.get(i).getTipo());

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            asistenciaObj.put(student1);
        }
        crearAsistencia();

    }


    public void crearAsistencia(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        params.put("grupo",id);
        params.put("fecha",date.toString(formatter));
        params.put("bimestre",idbimestre);
        params.put("asistencia",asistenciaObj.toString());
        client.setTimeout(300000);
        String url = config.getUrl()+config.getCreateAsistenciaSesion();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.post(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                        finish();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.asistencia_general, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();
                break;
            case R.id.action_play:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}

