package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by jesusnieves on 28/4/17.
 */
public class VerDatosActivity extends AppCompatActivity {

    Activity activity;
    private BarChart columnChartView,columnChartView2,columnChartView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.datos_ver);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Datos Bimestre");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        columnChartView = (BarChart) findViewById(R.id.datos_ver_chart);
        columnChartView2 = (BarChart) findViewById(R.id.datos_ver_chart2);
        columnChartView3 = (BarChart) findViewById(R.id.datos_ver_chart3);
        columnChartView.setDrawBarShadow(false);
        columnChartView.getDescription().setEnabled(false);
        columnChartView.setPinchZoom(false);
        columnChartView.setDrawGridBackground(false);
        columnChartView2.setDrawBarShadow(false);
        columnChartView2.getDescription().setEnabled(false);
        columnChartView2.setPinchZoom(false);
        columnChartView2.setDrawGridBackground(false);
        columnChartView3.setDrawBarShadow(false);
        columnChartView3.getDescription().setEnabled(false);
        columnChartView3.setPinchZoom(false);
        columnChartView3.setDrawGridBackground(false);
        generateDefaultData();
        generateDefaultData2();
        generateDefaultData3();
    }
    private void generateDefaultData() {
        List<BarEntry> values = new ArrayList<>();
        List<Integer> colors = new ArrayList<>();
        for(int i=0; i<5; i++){
            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            colors.add(color);
            values.add(new BarEntry(i,(float) Math.random() * 20f + 5));
        }
        BarDataSet set1 = new BarDataSet(values,"");
        set1.setDrawIcons(false);
        set1.setColors(colors);
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        BarData data = new BarData(dataSets);
        data.setValueTextSize(10f);
        data.setBarWidth(0.9f);
        columnChartView.setData(data);

    }
    private void generateDefaultData2() {
        List<BarEntry> values = new ArrayList<>();
        for(int i=0; i<5; i++){
            values.add(new BarEntry(i,(float) Math.random() * 20f + 5));
        }
        BarDataSet set1 = new BarDataSet(values,"");
        set1.setDrawIcons(false);
        set1.setColors(ColorTemplate.MATERIAL_COLORS);
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        BarData data = new BarData(dataSets);
        data.setValueTextSize(10f);
        data.setBarWidth(0.9f);
        columnChartView2.setData(data);

    }
    private void generateDefaultData3() {
        List<BarEntry> values = new ArrayList<>();
        for(int i=0; i<5; i++){
            values.add(new BarEntry(i,(float) Math.random() * 20f + 5));
        }
        BarDataSet set1 = new BarDataSet(values,"");
        set1.setDrawIcons(false);
        set1.setColors(ColorTemplate.MATERIAL_COLORS);
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        BarData data = new BarData(dataSets);
        data.setValueTextSize(10f);
        data.setBarWidth(0.9f);
        columnChartView3.setData(data);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.alumnos, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();

                break;
            case R.id.action_play:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}