package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Adapters.GruposAdapter;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.GruposItem;
import com.cucinestudios.mastertoolsmaestros.Models.GruposNumerosItem;
import com.cucinestudios.mastertoolsmaestros.Models.Tutorial;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    FloatingActionButton crear_grupos;
    FloatingActionButton crear_taller;
    FloatingActionButton archivados;
    FloatingActionButton activos;
    RecyclerView lista;
    RecyclerView.LayoutManager manager;
    Activity activity;
    ArrayList<GruposNumerosItem> gruposNumerosItems;
    ArrayList<GruposItem> gruposItems;
    GruposAdapter gruposAdapter;
    Config config;
    Realm realm;
    User user;
    TrailingCircularDotsLoader loading;
    SwipeRefreshLayout refreshLayout;
    TextView nombre, sub;
    String idGrupo;
    FloatingActionMenu menu;
    FrameLayout play;
    String url = "https://www.youtube.com/watch?v=hUMWoatmZ80";
    boolean first = false;
    Button listo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Grupos");
        play = (FrameLayout) toolbar.findViewById(R.id.alumnos_play);
        setSupportActionBar(toolbar);
        activity = this;
        realm = Realm.getDefaultInstance();
        config = new Config();
        crear_grupos = (FloatingActionButton) findViewById(R.id.main_grupos);
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        crear_taller = (FloatingActionButton) findViewById(R.id.main_taller);
        archivados = (FloatingActionButton) findViewById(R.id.main_archivados);
        activos = (FloatingActionButton) findViewById(R.id.main_activos);
        menu = (FloatingActionMenu) findViewById(R.id.menu);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.main_refresh);
        lista = (RecyclerView) findViewById(R.id.main_lista);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            idGrupo = bundle.getString("idGrupo");
        }


        crear_grupos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu.close(true);
                Intent intent = new Intent(activity,NuevoGrupoActivity.class);
                startActivity(intent);
            }
        });


        crear_taller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu.close(true);
                Intent intent = new Intent(activity,NuevoTallerActivity.class);
                startActivity(intent);
            }
        });


        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,VideoActivity.class);
                intent.putExtra("url",url);
                startActivity(intent);
            }
        });
        archivados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu.close(true);
                getListaArchivados();
            }
        });
        activos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu.close(true);
                getRefresh();
            }
        });

        refreshLayout.setColorSchemeColors(ContextCompat.getColor(activity,R.color.azul_oscuro),ContextCompat.getColor(activity,R.color.azul),ContextCompat.getColor(activity,R.color.naranja),ContextCompat.getColor(activity,R.color.verde),ContextCompat.getColor(activity,R.color.rojo));

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRefresh();
            }
        });
        lista.setHasFixedSize(true);
        manager = new LinearLayoutManager(activity);
        lista.setLayoutManager(manager);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        nombre = (TextView) navigationView.getHeaderView(0).findViewById(R.id.header_nombre);
        sub = (TextView) navigationView.getHeaderView(0).findViewById(R.id.header_sub);
        User user = realm.where(User.class).findFirst();
        nombre.setText("");
        sub.setText(user.getEmail());
        navigationView.setNavigationItemSelectedListener(this);
        getLista();
        lista.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if (dy > 0 && menu.getVisibility() == View.VISIBLE) {
                    menu.setVisibility(View.GONE);
                }else{
                    if (dy < 0 && menu.getVisibility() == View.GONE) {
                        menu.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState)
            {

                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        menu.close(true);
    }

    public void getLista(){
        gruposItems = new ArrayList<>();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        String url = config.getUrl()+config.getGrupos();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray grupos = data.getJSONArray("group");
                        for (int i = 0; i<grupos.length(); i++){
                            gruposNumerosItems = new ArrayList<>();
                            gruposNumerosItems.add(new GruposNumerosItem(1, "1",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(2, "2",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(3, "3",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(4, "4",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(5, "5",grupos.getJSONObject(i).getString("IDGrupo")));
                            String grado = grupos.getJSONObject(i).getString("Grado") +grupos.getJSONObject(i).getString("Grupo");
                            String escuela = grupos.getJSONObject(i).getString("Escuela") +"("+grupos.getJSONObject(i).getString("Ciclo")+")";
                            gruposItems.add(new GruposItem(grupos.getJSONObject(i).getString("IDGrupo"), grado, grupos.getJSONObject(i).getString("Materia"), gruposNumerosItems, escuela,grupos.getJSONObject(i).getString("Color"),false));
                        }
                        gruposAdapter = new GruposAdapter(activity,gruposItems);
                        lista.setAdapter(gruposAdapter);
                        getTaller();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                    first = true;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                first = true;
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });


    }
    public void getListaArchivados(){
        gruposItems.clear();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        String url = config.getUrl()+config.getArchivados();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray grupos = data.getJSONArray("group");
                        for (int i = 0; i<grupos.length(); i++){
                            gruposNumerosItems = new ArrayList<>();
                            gruposNumerosItems.add(new GruposNumerosItem(1, "1",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(2, "2",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(3, "3",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(4, "4",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(5, "5",grupos.getJSONObject(i).getString("IDGrupo")));
                            String grado = grupos.getJSONObject(i).getString("Grado") +grupos.getJSONObject(i).getString("Grupo");
                            String escuela = grupos.getJSONObject(i).getString("Escuela") +"("+grupos.getJSONObject(i).getString("Ciclo")+")";
                            gruposItems.add(new GruposItem(grupos.getJSONObject(i).getString("IDGrupo"), grado, grupos.getJSONObject(i).getString("Materia"), gruposNumerosItems, escuela,grupos.getJSONObject(i).getString("Color"),false));
                        }
                        if (archivados.getVisibility() == View.VISIBLE){
                            archivados.setVisibility(View.GONE);
                        }
                        if (crear_grupos.getVisibility() == View.VISIBLE){
                            crear_grupos.setVisibility(View.GONE);
                        }
                        if (crear_taller.getVisibility() == View.VISIBLE){
                            crear_taller.setVisibility(View.GONE);
                        }
                        if (activos.getVisibility() == View.GONE){
                            activos.setVisibility(View.VISIBLE);
                        }
                        gruposAdapter.notifyDataSetChanged();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                    Toast.makeText(activity,"No hay grupos archivados",Toast.LENGTH_SHORT).show();
                    first = true;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                first = true;
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });


    }

    public void getTaller(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        String url = config.getUrl()+config.getTalleres();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
                first = true;

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray grupos = data.getJSONArray("group");
                        for (int i = 0; i<grupos.length(); i++){
                            gruposNumerosItems = new ArrayList<>();
                            gruposNumerosItems.add(new GruposNumerosItem(1, "1",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(2, "2",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(3, "3",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(4, "4",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(5, "5",grupos.getJSONObject(i).getString("IDGrupo")));
                            String grado = grupos.getJSONObject(i).getString("Grado") +grupos.getJSONObject(i).getString("Grupo");
                            String escuela = grupos.getJSONObject(i).getString("Escuela") +"("+grupos.getJSONObject(i).getString("Ciclo")+")";
                            gruposItems.add(new GruposItem(grupos.getJSONObject(i).getString("IDGrupo"), grado, grupos.getJSONObject(i).getString("Materia"), gruposNumerosItems, escuela,grupos.getJSONObject(i).getString("Color"),true));
                        }
                        gruposAdapter.notifyDataSetChanged();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });


    }

    public void getTaller2(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        String url = config.getUrl()+config.getTalleres();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, new AsyncHttpResponseHandler() {


            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray grupos = data.getJSONArray("group");
                        for (int i = 0; i<grupos.length(); i++){
                            gruposNumerosItems = new ArrayList<>();
                            gruposNumerosItems.add(new GruposNumerosItem(1, "1",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(2, "2",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(3, "3",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(4, "4",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(5, "5",grupos.getJSONObject(i).getString("IDGrupo")));
                            String grado = grupos.getJSONObject(i).getString("Grado") +grupos.getJSONObject(i).getString("Grupo");
                            String escuela = grupos.getJSONObject(i).getString("Escuela") +"("+grupos.getJSONObject(i).getString("Ciclo")+")";
                            gruposItems.add(new GruposItem(grupos.getJSONObject(i).getString("IDGrupo"), grado, grupos.getJSONObject(i).getString("Materia"), gruposNumerosItems, escuela,grupos.getJSONObject(i).getString("Color"),true));
                        }
                        gruposAdapter.notifyDataSetChanged();
                        refreshLayout.setRefreshing(false);
                    }else {
                        refreshLayout.setRefreshing(false);
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    refreshLayout.setRefreshing(false);
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                refreshLayout.setRefreshing(false);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });


    }

    public void getRefresh(){
        gruposItems.clear();
        gruposAdapter.notifyDataSetChanged();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        String url = config.getUrl()+config.getGrupos();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, new AsyncHttpResponseHandler() {


            @Override
            public void onStart() {
                super.onStart();
                refreshLayout.setRefreshing(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray grupos = data.getJSONArray("group");
                        for (int i = 0; i<grupos.length(); i++){
                            gruposNumerosItems = new ArrayList<>();
                            gruposNumerosItems.add(new GruposNumerosItem(1, "1",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(2, "2",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(3, "3",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(4, "4",grupos.getJSONObject(i).getString("IDGrupo")));
                            gruposNumerosItems.add(new GruposNumerosItem(5, "5",grupos.getJSONObject(i).getString("IDGrupo")));
                            String grado = grupos.getJSONObject(i).getString("Grado") +grupos.getJSONObject(i).getString("Grupo");
                            String escuela = grupos.getJSONObject(i).getString("Escuela") +"("+grupos.getJSONObject(i).getString("Ciclo")+")";
                            gruposItems.add(new GruposItem(grupos.getJSONObject(i).getString("IDGrupo"), grado, grupos.getJSONObject(i).getString("Materia"), gruposNumerosItems, escuela,grupos.getJSONObject(i).getString("Color"),false));
                        }
                        gruposAdapter.notifyDataSetChanged();
                        if (archivados.getVisibility() == View.GONE){
                            archivados.setVisibility(View.VISIBLE);
                        }
                        if (crear_grupos.getVisibility() == View.GONE){
                            crear_grupos.setVisibility(View.VISIBLE);
                        }
                        if (crear_taller.getVisibility() == View.GONE){
                            crear_taller.setVisibility(View.VISIBLE);
                        }
                        if (activos.getVisibility() == View.VISIBLE){
                            activos.setVisibility(View.GONE);
                        }
                        getTaller2();
                    }else {
                        refreshLayout.setRefreshing(false);
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    refreshLayout.setRefreshing(false);
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                refreshLayout.setRefreshing(false);

                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (menu != null){
            menu.close(true);
        }
        if(gruposItems != null && gruposAdapter != null && first) {
            getRefresh();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.grupos) {
            // Handle the camera action
        } else if (id == R.id.contacto) {
            String url = "http://mastertools.mx/contacto.php#content";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } else if ((id == R.id.pagina)) {
            String url = "http://www.mastertools.mx";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } else if (id == R.id.cerrar) {
            realm.beginTransaction();
            realm.delete(User.class);
            realm.delete(Tutorial.class);
            realm.commitTransaction();
            Intent intent = new Intent(activity, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
