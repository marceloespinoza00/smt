package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Adapters.ExamenSectionAdapter;
import com.cucinestudios.mastertoolsmaestros.Adapters.InstrumentoSectionAdapter;
import com.cucinestudios.mastertoolsmaestros.Adapters.TrabajoAdapter;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.ExamenChildItem;
import com.cucinestudios.mastertoolsmaestros.Models.ExamenParentItem;
import com.cucinestudios.mastertoolsmaestros.Models.GenericoItem;
import com.cucinestudios.mastertoolsmaestros.Models.TrabajoItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;


/**
 * Created by niniparra on 6/3/17.
 */
public class AlumnoActivity  extends AppCompatActivity
{
    EditText nombre, apellido,curp;
    EditText nec;
    Config config;
    Activity activity;;
    ArrayList<GenericoItem> necItems;
    LinearLayout examen,habilidades,control;
    LinearLayout asistencia,trabajo,instrumento;
    ImageView editar, eliminar;
    String id;
    String idBimestre;
    String idGrupo;
    int bimestre;
    TrailingCircularDotsLoader loading;
    Realm realm;
    AlertDialog alertDialog;
    TextView semAsistencia, semTrabajo, semInstrumentos, semExamenes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alumno);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        editar = (ImageView) toolbar.findViewById(R.id.alumno_editar);
        eliminar = toolbar.findViewById(R.id.alumno_eliminar);
        semAsistencia = findViewById(R.id.sem_asistencia);
        semTrabajo = findViewById(R.id.sem_trabajo);
        semInstrumentos = findViewById(R.id.sem_instrumentos);
        semExamenes = findViewById(R.id.sem_examenes);
        toolbar.setTitle("Datos Generales");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config = new Config();
        realm = Realm.getDefaultInstance();
        nombre = (EditText) findViewById(R.id.alumno_nombre);
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        apellido = (EditText) findViewById(R.id.alumno_apellido);
        curp = (EditText) findViewById(R.id.alumno_curp);
        nec = (EditText) findViewById(R.id.alumno_nec);
        asistencia =  findViewById(R.id.alumno_asistencia);
        trabajo =  findViewById(R.id.alumno_trabajo);
        instrumento = findViewById(R.id.alumno_instrumento);
        examen =  findViewById(R.id.alumno_examen);
        habilidades =  findViewById(R.id.alumno_habilidades);
        control =  findViewById(R.id.alumno_control);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
            idBimestre = bundle.getString("idBimestre");
            idGrupo = bundle.getString("idGrupo");
            bimestre = bundle.getInt("bimestre");
        }
        examen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,ExamenActivity.class);
                intent.putExtra("id",id);
                intent.putExtra("idBimestre",idBimestre);
                intent.putExtra("idGrupo",idGrupo);
                startActivity(intent);
            }
        });
        asistencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,AsistenciasActivity.class);
                intent.putExtra("id",id);
                intent.putExtra("idBimestre",idBimestre);
                intent.putExtra("idGrupo",idGrupo);
                intent.putExtra("bimestre",bimestre);
                startActivity(intent);
            }
        });
        instrumento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,InstrumentosActivity.class);
                intent.putExtra("id",id);
                intent.putExtra("idBimestre",idBimestre);
                startActivity(intent);
            }
        });
        trabajo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,TrabajoActivity.class);
                intent.putExtra("id",id);
                intent.putExtra("idBimestre",idBimestre);
                intent.putExtra("idGrupo",idGrupo);
                startActivity(intent);
            }
        });
        habilidades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,HabilidadesActivity.class);
                intent.putExtra("id",id);
                intent.putExtra("idBimestre",idBimestre);
                intent.putExtra("idGrupo",idGrupo);
                startActivity(intent);
            }
        });
        control.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(activity,ControlActivity.class);
                intent.putExtra("id",id);
                intent.putExtra("bimestre",bimestre);
                intent.putExtra("grupo",idGrupo);
                startActivity(intent);
            }
        });
        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,EditarAlumnoActivity.class);
                intent.putExtra("id",id);
                intent.putExtra("idGrupo",idGrupo);
                startActivity(intent);
            }
        });
        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog = new AlertDialog.Builder(AlumnoActivity.this, R.style.MyDialogTheme)
                        .setCancelable(false)
                        .setTitle("Advertencia!")
                        .setMessage("Seguro que desea eliminar a "+nombre.getText()+" "+apellido.getText()+"?")
                        .setPositiveButton("Si",null)
                        .setNegativeButton("No",null)
                        .create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        Button yesButton = (alertDialog).getButton(android.app.AlertDialog.BUTTON_POSITIVE);
                        Button noButton = (alertDialog).getButton(android.app.AlertDialog.BUTTON_NEGATIVE);
                        yesButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                eliminarAlumno();
                            }
                        });
                        noButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();
                            }
                        });
                    }
                });
                alertDialog.show();
            }
        });
        agregarNec();
        getDetalle();
        semaforoAsistencia();
        semaforoTrabajos();
        semaforoInstrumentos();
        semaforoExamentes();
    }

    public void getDetalle(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        String url = config.getUrl()+config.getAlumno();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url,params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONObject alumno = data.getJSONObject("alumno");
                        String isnec = "No";
                        if (alumno.getBoolean("EsUSAER")){
                            isnec = "Si";
                        }
                        int posnec = getPosNEC(isnec);
                        nec.setText(necItems.get(posnec).getName());
                        agregarNec();
                        String lastname = alumno.getString("ApellidoPaterno")+" "+alumno.getString("ApellidoMaterno");
                        nombre.setText(alumno.getString("Nombre"));
                        apellido.setText(lastname);
                        curp.setText(alumno.getString("Curp"));


                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });

    }

    public int getPosNEC(String codigo){
        int pos = 0;
        for (int i=0; i<necItems.size();i++){
            if (necItems.get(i).getName().matches(codigo)){
                pos = i;
            }
        }
        return pos;
    }

    public void agregarNec(){
        necItems = new ArrayList<>();
        necItems.add(new GenericoItem(1,"Si"));
        necItems.add(new GenericoItem(2,"No"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDetalle();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.alumno, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void eliminarAlumno(){
        AsyncHttpClient client = new AsyncHttpClient();
        User user = realm.where(User.class).findFirst();
        RequestParams params = new RequestParams();
        params.put("idAlumno",id);
        String url = config.getUrl()+config.getDeleteAlumno();
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.delete(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Toast.makeText(activity,"Alumno eliminado correctamente",Toast.LENGTH_SHORT).show();
                alertDialog.dismiss();
                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void semaforoAsistencia(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        client.setTimeout(300000);
        String url = config.getUrl()+config.getShowAsistenciaAlumno();
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        final int inasistencias = data.getInt("inasistencias");
                        if(inasistencias<2){
                            semAsistencia.setBackgroundResource(R.drawable.semaforo_rojo);
                        } else if(inasistencias<=4){
                            semAsistencia.setBackgroundResource(R.drawable.semaforo_amarillo);
                        } else{
                            semAsistencia.setBackgroundResource(R.drawable.semaforo_verde);
                        }
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void semaforoTrabajos(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        String url = config.getUrl()+config.getTrabajosAlumnos();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        Log.i("gettoken","gettoken"+user.getToken()+" "+id+" "+idBimestre);
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    double entregados = 0;
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1) {
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray trabajos = data.getJSONArray("trabajos");
                        Log.e("al",trabajos.length()+"");
                        for (int i = 0; i<trabajos.length(); i++){
                            int estado = trabajos.getJSONObject(i).getInt("Estado");
                            if(estado==1){
                                entregados++;
                            }
                        }
                        double total = trabajos.length();
                        double promTrabajos = (entregados/total)*100;
                        int prom = (int) promTrabajos;
                        if(prom<=60){
                            semTrabajo.setBackgroundResource(R.drawable.semaforo_rojo);
                        }else if(promTrabajos<90){
                            semTrabajo.setBackgroundResource(R.drawable.semaforo_amarillo);
                        }else{
                            semTrabajo.setBackgroundResource(R.drawable.semaforo_verde);
                        }

                    }else {

                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }

    public void semaforoInstrumentos(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        final RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        client.setTimeout(300000);
        String url = config.getUrl()+config.getInstrumentos();
        Log.e("url",url+params.toString());
        Log.i("bearerrr","bearerr"+user.getToken());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try{
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray instrumentos = data.getJSONArray("instrumentos");
                        double sumPromedio = 0;
                        Log.e("al",instrumentos.length()+"");
                        for(int i = 0; i<instrumentos.length(); i++){
                            double promedio = round(instrumentos.getJSONObject(i).getDouble("promedio"),1);
                            if(promedio<5) {
                                sumPromedio = sumPromedio + 5;
                            }else if (promedio>10){
                                sumPromedio = sumPromedio + 10;
                            }else{
                                sumPromedio = sumPromedio + promedio;
                            }
                        }
                        double totalInstrumentos = instrumentos.length();
                        double promInstrumentos = (sumPromedio/totalInstrumentos)*10;
                        int enteroInstrumentos = (int) promInstrumentos;
                        if(enteroInstrumentos<=60){
                            semInstrumentos.setBackgroundResource(R.drawable.semaforo_rojo);
                        }else if(enteroInstrumentos<90){
                            semInstrumentos.setBackgroundResource(R.drawable.semaforo_amarillo);
                        }else{
                            semInstrumentos.setBackgroundResource(R.drawable.semaforo_verde);
                        }

                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("einstrumentos",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }

    public void semaforoExamentes(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        client.setTimeout(300000);
        String url = config.getUrl()+config.getExamenes();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {


            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try{
                    JSONObject respuesta = new JSONObject(new String(response));
                    double acumPromedio = 0;
                    double totalExamenes = 0, promExamenes = 0;
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray examenes = data.getJSONArray("examenes");
                        for(int i = 0; i<examenes.length(); i++) {

                            JSONArray temas = examenes.getJSONObject(i).getJSONArray("temas");

                            String titulo = examenes.getJSONObject(i).getString("titulo");
                            String fecha = examenes.getJSONObject(i).getString("fechaEntrega");
                            String des = examenes.getJSONObject(i).getString("tipo");

                            DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                            DateTime temp = df.withOffsetParsed().parseDateTime(fecha);
                            DateTimeFormatter df2 = DateTimeFormat.forPattern("yyyy-MM-dd");
                            Log.i("examen","examen"+examenes.getJSONObject(i));
                            double promedio = 0;
                            if (!examenes.getJSONObject(i).isNull("promediototal")) {
                                promedio = round(examenes.getJSONObject(i).getDouble("promediototal"), 1);
                            }
                            acumPromedio = acumPromedio + promedio;

                        }
                        totalExamenes = examenes.length();
                        promExamenes = (acumPromedio/totalExamenes)*10;
                        if(promExamenes<=60){
                            semExamenes.setBackgroundResource(R.drawable.semaforo_rojo);
                        }else if(promExamenes<90){
                            semExamenes.setBackgroundResource(R.drawable.semaforo_amarillo);
                        }else{
                            semExamenes.setBackgroundResource(R.drawable.semaforo_verde);
                        }

                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());

                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}

