package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Adapters.AsisAdapter;
import com.cucinestudios.mastertoolsmaestros.Models.AsistenciaGeneralItem;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.GenericoItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import ru.slybeaver.slycalendarview.SlyCalendarDialog;

/**
 * Created by jesusnieves on 14/3/17.
 */
public class  AsistenciasActivity  extends AppCompatActivity implements SlyCalendarDialog.Callback{

    TextView dia;
    MaterialBetterSpinner tipo;
    ArrayAdapter<GenericoItem> tipoAdapter;
    ArrayList<GenericoItem> tipoItems;
    Activity activity;
    Config config;
    Realm realm;
    TrailingCircularDotsLoader loading;
    int seleccionado = -1;
    Button listo;
    String date ="";
    String idBimestre;
    String idGrupo;
    String id;
    int bimestre;
    FrameLayout play;
    TextView asis, inasis;
    FloatingActionButton general;
    RecyclerView lista;
    ArrayList<AsistenciaGeneralItem> asistencia_generalItems;
    AsisAdapter asistenciaAdapter;
    private static final String FRAG_TAG_DATE_PICKER = "fragment_date_picker_name";
    JSONArray asistenciaObj;
    String url = "https://www.youtube.com/watch?v=TZQLhrIlhXg";
    TextView semAsistencia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asistencias);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        play = (FrameLayout) toolbar.findViewById(R.id.asistencias_play);
        semAsistencia = findViewById(R.id.semaforoAsistencia);
        toolbar.setTitle("Asistencia");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config= new Config();
        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        dia = (TextView) findViewById(R.id.asistencias_dia);
        asis = (TextView) findViewById(R.id.asistencias_asis);
        inasis = (TextView) findViewById(R.id.asistencias_inasis);
        tipo = (MaterialBetterSpinner) findViewById(R.id.asistencias_tipo);
        listo = (Button) findViewById(R.id.asistencias_listo);
        lista = findViewById(R.id.asistencias_lista);
        lista.setNestedScrollingEnabled(false);
        LinearLayoutManager manager = new LinearLayoutManager(activity);
        lista.setLayoutManager(manager);
        general = (FloatingActionButton) findViewById(R.id.asistencia_agregar);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
            idBimestre = bundle.getString("idBimestre");
            idGrupo = bundle.getString("idGrupo");
            bimestre = bundle.getInt("bimestre");
        }
        general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,AsistenciaGeneralActivity.class);
                intent.putExtra("bimestre",bimestre);
                intent.putExtra("id",idGrupo);
                startActivity(intent);
            }
        });
        listo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });
        dia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SlyCalendarDialog()
                        .setSingle(true)
                        .setBackgroundColor(ContextCompat.getColor(activity, R.color.blanco))
                        .setHeaderTextColor(ContextCompat.getColor(activity, R.color.blanco))
                        .setHeaderColor(ContextCompat.getColor(activity, R.color.colorPrimary))
                        .setSelectedTextColor(ContextCompat.getColor(activity, R.color.blanco))
                        .setTextColor(ContextCompat.getColor(activity, R.color.negro))
                        .setCallback(AsistenciasActivity.this)
                        .show(getSupportFragmentManager(), FRAG_TAG_DATE_PICKER);
            }
        });


        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,VideoActivity.class);
                intent.putExtra("url",url);
                startActivity(intent);
            }
        });

        agregarTipo();
        getLista();

    }

    void submit(){
        if (!validateFecha()){
            return;
        }
        if (!validateSeleccion()){
            return;
        }
        JSONObject student1 = new JSONObject();
        try {
            student1.put("IDAlumno", id);
            student1.put("Estado", seleccionado);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        asistenciaObj = new JSONArray();
        asistenciaObj.put(student1);
        crearAsistencia();

    }

    private boolean validateSeleccion() {
        if (seleccionado == -1) {
            tipo.setError("No puede estar vacio");
            requestFocus(tipo);
            return false;
        }
        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private boolean validateFecha() {
        if (date.trim().isEmpty()) {
            Toast.makeText(activity,"Debes agregar una fecha ",Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    public void changeAsistencia(String sesion, String alumno, int estado){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("sesion",sesion);
        params.put("alumno",alumno);
        params.put("estado",estado);
        client.setTimeout(300000);
        String url = config.getUrl()+config.getUpdateAsistencia();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.put(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        getListaRefresh();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }


    public void crearAsistencia(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("grupo",idGrupo);
        params.put("fecha",date);
        params.put("bimestre",idBimestre);
        params.put("asistencia",asistenciaObj.toString());
        client.setTimeout(300000);
        String url = config.getUrl()+config.getCreateAsistenciaSesion();
        Log.e("aqui","aqui");
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.post(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        dia.setText("Agregar dia");
                        seleccionado = -1;
                        tipo.setText("");
                        agregarTipo();
                        getListaRefresh();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }


    public void getListaRefresh(){
        asistencia_generalItems.clear();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        client.setTimeout(300000);
        String url = config.getUrl()+config.getShowAsistenciaAlumno();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray alumnos = data.getJSONArray("data");
                        final int asistencias = data.getInt("asistencias");
                        final int inasistencias = data.getInt("inasistencias");
                        Log.e("al",alumnos.length()+"");
                        asis.setText(asistencias+"");
                        inasis.setText(inasistencias+"");

                        for (int i = 0; i<alumnos.length(); i++) {
                            String hora = alumnos.getJSONObject(i).getString("Fecha");
                            int tipo = alumnos.getJSONObject(i).getInt("Estado");

                            DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                            DateTime temp = df.withOffsetParsed().parseDateTime(hora);
                            DateTimeFormatter df2 = DateTimeFormat.forPattern("yyyy-MM-dd");

                            asistencia_generalItems.add(new AsistenciaGeneralItem(alumnos.getJSONObject(i).getString("IDSesion"),temp.toString(df2),tipo,alumnos.getJSONObject(i).getString("IDAlumno")));
                        }
                        Log.e("al",asistencia_generalItems.size()+"");
                        asistenciaAdapter.notifyDataSetChanged();
                        if(inasistencias<2){
                            semAsistencia.setBackgroundResource(R.drawable.semaforo_verde);
                        } else if(inasistencias<=4){
                            semAsistencia.setBackgroundResource(R.drawable.semaforo_amarillo);
                        } else{
                            semAsistencia.setBackgroundResource(R.drawable.semaforo_rojo);
                        }

                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }


    public void getLista(){
        asistencia_generalItems = new ArrayList<>();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        client.setTimeout(300000);
        String url = config.getUrl()+config.getShowAsistenciaAlumno();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray alumnos = data.getJSONArray("data");
                        final int asistencias = data.getInt("asistencias");
                        final int inasistencias = data.getInt("inasistencias");
                        Log.e("al",alumnos.length()+"");
                        asis.setText(asistencias+"");
                        inasis.setText(inasistencias+"");

                        for (int i = 0; i<alumnos.length(); i++) {
                            String hora = alumnos.getJSONObject(i).getString("Fecha");
                            int tipo = alumnos.getJSONObject(i).getInt("Estado");

                            DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                            DateTime temp = df.withOffsetParsed().parseDateTime(hora);
                            DateTimeFormatter df2 = DateTimeFormat.forPattern("yyyy-MM-dd");

                            asistencia_generalItems.add(new AsistenciaGeneralItem(alumnos.getJSONObject(i).getString("IDSesion"),temp.toString(df2),tipo,alumnos.getJSONObject(i).getString("IDAlumno")));
                        }
                        Log.e("al",asistencia_generalItems.size()+"");
                        asistenciaAdapter = new AsisAdapter(activity,asistencia_generalItems);
                        lista.setAdapter(asistenciaAdapter);
                        if(inasistencias<2){
                            semAsistencia.setBackgroundResource(R.drawable.semaforo_verde);
                        } else if(inasistencias<=4){
                            semAsistencia.setBackgroundResource(R.drawable.semaforo_amarillo);
                        } else{
                            semAsistencia.setBackgroundResource(R.drawable.semaforo_rojo);
                        }

                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }


    public void agregarTipo(){
        tipoItems = new ArrayList<>();
        tipoItems.add(new GenericoItem(1,"Falta"));
        tipoItems.add(new GenericoItem(2,"Asistencia"));
        tipoItems.add(new GenericoItem(3,"Retardo"));
        tipoItems.add(new GenericoItem(4,"Suspension"));
        tipoItems.add(new GenericoItem(5,"Justificacion"));
        tipoAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_dropdown_item_1line, tipoItems);
        tipo.setAdapter(tipoAdapter);
        tipo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                seleccionado = position;
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.alumnos, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCancelled() {

    }

    @Override
    public void onDataSelected(Calendar firstDate, Calendar secondDate, int hours, int minutes) {
        if (firstDate != null) {
            TimeZone tz = firstDate.getTimeZone();
            DateTimeZone jodaTz = DateTimeZone.forID(tz.getID());
            DateTime dateTime = new DateTime(firstDate.getTimeInMillis(), jodaTz);
            dia.setText(dateTime.getDayOfMonth() + "/" + (dateTime.getMonthOfYear()) + "/" + dateTime.getYear());
            date = dateTime.getYear() + "/" + (dateTime.getMonthOfYear() + 1) + "/" + dateTime.getDayOfMonth();
            Log.e("e",date);
        }
    }
}

