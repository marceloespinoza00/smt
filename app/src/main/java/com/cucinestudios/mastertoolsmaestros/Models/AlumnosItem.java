package com.cucinestudios.mastertoolsmaestros.Models;

/**
 * Created by niniparra on 2/3/17.
 */
public class AlumnosItem {
    String id;
    String name;

    public AlumnosItem(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
