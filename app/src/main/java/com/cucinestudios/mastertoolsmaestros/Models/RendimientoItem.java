package com.cucinestudios.mastertoolsmaestros.Models;

/**
 * Created by niniparra on 26/4/17.
 */
public class RendimientoItem {
    String nombre;
    int color;

    public RendimientoItem(String nombre, int color) {
        this.nombre = nombre;
        this.color = color;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

}
