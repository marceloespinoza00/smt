package com.cucinestudios.mastertoolsmaestros.Models;

/**
 * Created by niniparra on 26/4/17.
 */
public class ControlItem {
    String id;
    String tema;
    String cali;
    String por;

    public ControlItem(String id, String tema, String cali, String por) {
        this.id = id;
        this.tema = tema;
        this.cali = cali;
        this.por = por;
    }

    public ControlItem() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getCali() {
        return cali;
    }

    public void setCali(String cali) {
        this.cali = cali;
    }

    public String getPor() {
        return por;
    }

    public void setPor(String por) {
        this.por = por;
    }
}
