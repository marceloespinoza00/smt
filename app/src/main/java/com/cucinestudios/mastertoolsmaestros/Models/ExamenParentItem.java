package com.cucinestudios.mastertoolsmaestros.Models;


import java.util.List;

/**
 * Created by jesusnieves on 8/3/17.
 */

public class ExamenParentItem  {
    String id;
    String parcial;
    String fecha;
    String des;
    String id2;
    String group;

    public ExamenParentItem(String id, String parcial, String fecha, String des, String id2, String group) {
        this.id = id;
        this.parcial = parcial;
        this.fecha = fecha;
        this.des = des;
        this.id2 = id2;
        this.group = group;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId2() {
        return id2;
    }

    public void setId2(String id2) {
        this.id2 = id2;
    }

    public String getParcial() {
        return parcial;
    }

    public void setParcial(String parcial) {
        this.parcial = parcial;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }


    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExamenParentItem)) return false;

        ExamenParentItem examenParentItem = (ExamenParentItem) o;

        return getId() == examenParentItem.getId();

    }

    @Override
    public int hashCode() {
        return 0;
    }


}
