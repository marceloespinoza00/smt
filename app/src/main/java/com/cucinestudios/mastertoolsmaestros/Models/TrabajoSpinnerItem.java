package com.cucinestudios.mastertoolsmaestros.Models;

/**
 * Created by jesusnieves on 19/5/17.
 */
public class TrabajoSpinnerItem {
    int id;
    String tipo;
    int type;

    public TrabajoSpinnerItem(int id, String tipo, int type) {
        this.id = id;
        this.tipo = tipo;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
