package com.cucinestudios.mastertoolsmaestros.Models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nianya on 15/08/17.
 */

public class User extends RealmObject {
    @PrimaryKey
    String id;
    String email;
    String token;

    public String getId(){
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
