package com.cucinestudios.mastertoolsmaestros.Models;

import java.util.ArrayList;

/**
 * Created by niniparra on 23/2/17.
 */
public class GruposItem {
    String id;
    String seccion;
    String nombre;
    ArrayList<GruposNumerosItem> numeros;
    String escuela;
    String color;
    boolean taller;

    public GruposItem(String id, String seccion, String nombre, ArrayList<GruposNumerosItem> numeros, String escuela, String color, boolean taller) {
        this.id = id;
        this.seccion = seccion;
        this.nombre = nombre;
        this.numeros = numeros;
        this.escuela = escuela;
        this.color = color;
        this.taller = taller;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isTaller() {
        return taller;
    }

    public void setTaller(boolean taller) {
        this.taller = taller;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<GruposNumerosItem> getNumeros() {
        return numeros;
    }

    public void setNumeros(ArrayList<GruposNumerosItem> numeros) {
        this.numeros = numeros;
    }

    public String getEscuela() {
        return escuela;
    }

    public void setEscuela(String escuela) {
        this.escuela = escuela;
    }
}
