package com.cucinestudios.mastertoolsmaestros.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jesusnieves on 8/3/17.
 */
public class ExamenChildItem {

    String id;
    String tema;
    String calif;
    Boolean last;
    String aspecto;

    public ExamenChildItem(String id, String tema, String calif, Boolean last, String aspecto) {
        this.id = id;
        this.tema = tema;
        this.calif = calif;
        this.last = last;
        this.aspecto = aspecto;
    }

    public Boolean getLast() {
        return last;
    }

    public void setLast(Boolean last) {
        this.last = last;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getCalif() {
        return calif;
    }

    public void setCalif(String calif) {
        this.calif = calif;
    }

    public String getAspecto() {
        return aspecto;
    }

    public void setAspecto(String aspecto) {
        this.aspecto = aspecto;
    }

    protected ExamenChildItem(Parcel in) {
        tema = in.readString();
    }

}
