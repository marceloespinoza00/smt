package com.cucinestudios.mastertoolsmaestros.Models;

/**
 * Created by niniparra on 23/2/17.
 */
public class GruposNumerosItem {
    int id;
    String name;
    String id_grupo;

    public GruposNumerosItem(int id, String name, String id_grupo) {
        this.id = id;
        this.name = name;
        this.id_grupo = id_grupo;
    }

    public String getId_grupo() {
        return id_grupo;
    }

    public void setId_grupo(String id_grupo) {
        this.id_grupo = id_grupo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
