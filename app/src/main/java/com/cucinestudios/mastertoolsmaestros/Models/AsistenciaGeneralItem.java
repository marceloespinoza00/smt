package com.cucinestudios.mastertoolsmaestros.Models;

/**
 * Created by jesusnieves on 19/5/17.
 */
public class AsistenciaGeneralItem {
    String id;
    String nombre;
    int tipo;
    String alumno;

    public AsistenciaGeneralItem(String id, String nombre, int tipo, String alumno) {
        this.id = id;
        this.nombre = nombre;
        this.tipo = tipo;
        this.alumno = alumno;
    }

    public String getAlumno() {
        return alumno;
    }

    public void setAlumno(String alumno) {
        this.alumno = alumno;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
}
