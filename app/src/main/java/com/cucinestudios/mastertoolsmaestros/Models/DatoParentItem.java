package com.cucinestudios.mastertoolsmaestros.Models;


import java.util.List;

/**
 * Created by Nianya on 16/5/18.
 */

public class DatoParentItem {

    String bimestre;

    public DatoParentItem(String bimestre, List<DatoChildItem> items) {
        this.bimestre = bimestre;
    }

    public String getBimestre() {
        return bimestre;
    }

    public void setBimestre(String bimestre) {
        this.bimestre = bimestre;
    }
}