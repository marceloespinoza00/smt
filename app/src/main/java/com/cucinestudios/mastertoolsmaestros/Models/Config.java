package com.cucinestudios.mastertoolsmaestros.Models;

/**
 * Created by niniparra on 2/3/17.
 */
public class Config {
    String url = "http://54.235.235.17:9000/api/";
    String login = "login";
    String grupos = "grupos";
    String talleres = "talleres";
    String alumnos = "getAlumnosBimestre";
    String alumno = "alumno";
    String nuevogrupo ="nuevogrupo";
    String nuevotaller = "settaller";
    String nuevoalumno = "setalumno";
    String updateAlumno = "updateAlumno";
    String deleteAlumno = "deleteAlumno";
    String showAsistenciaAlumno = "showAsistenciaAlumno";
    String createAsistenciaSesion = "createAsistenciaSesion";
    String trabajosAlumnos = "trabajosalumnos";
    String updatetrabajoalumno = "updatetrabajoalumno";
    String grupo = "grupo";
    String updategrupo = "updategrupo";
    String habilidadesAlumno = "habilidadalumno";
    String crearHabilidadAlumno = "crearhabilidadalumno";
    String updateHabilidadAlumno = "updatehabilidadalumno";
    String examenes = "examenes";
    String examen = "examen";
    String nuevoTemaExamen = "temaexamen";
    String updateExamen = "updatetemaexamen";
    String instrumentos = "instrumentosalumno";
    String instrumentoAlumno = "instrumentoalumno";
    String updateInstrumentos = "instrumentosalumno";
    String updateAsistencia = "changeAsistenciaAlumno";
    String createTrabajo = "createtrabajo";
    String controlInstrumentos = "controlinstrumentos";
    String controlInstruBimiestre = "getcontrolinstrumentosbimestres";
    String changeStatusGrupo = "updategrupostatus";
    String clonarGrupo = "clonargrupo";
    String archivados = "gruposarchivados";


    public Config() {
    }

    public String getUrl(){
        return url;
    }

    public String getLogin() {
        return login;
    }

    public String getGrupos() {
        return grupos;
    }

    public String getTalleres() {
        return talleres;
    }

    public String getAlumnos() {
        return alumnos;
    }

    public String getAlumno() {
        return alumno;
    }

    public String getNuevogrupo() {
        return nuevogrupo;
    }

    public String getNuevotaller() {
        return nuevotaller;
    }

    public String getNuevoalumno() {
        return nuevoalumno;
    }

    public String getShowAsistenciaAlumno(){
        return showAsistenciaAlumno;
    }

    public String getCreateAsistenciaSesion(){
        return createAsistenciaSesion;
    }

    public String getTrabajosAlumnos(){
        return trabajosAlumnos;
    }

    public String getUpdateAlumno(){
        return updateAlumno;
    }

    public String getDeleteAlumno() {
        return deleteAlumno;
    }

    public String getUpdateTrabajoAlumno(){
        return updatetrabajoalumno;
    }

    public String getGrupo(){
        return grupo;
    }

    public String getUpdateGrupo(){
        return updategrupo;
    }

    public String getHabilidadesAlumno(){
        return habilidadesAlumno;
    }

    public String getCreateHabilidadesAlumno(){
        return crearHabilidadAlumno;
    }

    public String getUpdateHabilidadesAlumno(){
        return updateHabilidadAlumno;
    }

    public String getExamenes() {
        return examenes;
    }

    public String getExamen() {
        return examen;
    }

    public String getTemaExamen() {
        return nuevoTemaExamen;
    }

    public String getUpdateExamen() {
        return updateExamen;
    }

    public String getInstrumentos() {
        return instrumentos;
    }

    public String getInstrumentosAlumno() {
        return instrumentoAlumno;
    }

    public String getUpdateInstrumentos() {
        return updateInstrumentos;
    }

    public String getUpdateAsistencia() {
        return updateAsistencia;
    }

    public String getCreateTrabajo(){
        return createTrabajo;
    }

    public String getListControl(){
        return controlInstrumentos;
    }

    public String getControlBimestre(){
        return controlInstruBimiestre;
    }

    public String getStatusGrupo(){
        return changeStatusGrupo;
    }

    public String getClonarGrupo(){
        return clonarGrupo;
    }

    public String getArchivados() {
        return archivados;
    }
}
