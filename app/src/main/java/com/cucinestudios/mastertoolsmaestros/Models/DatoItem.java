package com.cucinestudios.mastertoolsmaestros.Models;

/**
 * Created by jesusnieves on 27/4/17.
 */
public class DatoItem {
    String id;
    String nombre;
    String uno;
    String dos;
    String tres;
    String cuatro;
    String cinco;

    public DatoItem(String id, String nombre, String uno, String dos, String tres, String cuatro, String cinco) {
        this.id = id;
        this.nombre = nombre;
        this.uno = uno;
        this.dos = dos;
        this.tres = tres;
        this.cuatro = cuatro;
        this.cinco = cinco;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUno() {
        return uno;
    }

    public void setUno(String uno) {
        this.uno = uno;
    }

    public String getDos() {
        return dos;
    }

    public void setDos(String dos) {
        this.dos = dos;
    }

    public String getTres() {
        return tres;
    }

    public void setTres(String tres) {
        this.tres = tres;
    }

    public String getCuatro() {
        return cuatro;
    }

    public void setCuatro(String cuatro) {
        this.cuatro = cuatro;
    }

    public String getCinco() {
        return cinco;
    }

    public void setCinco(String cinco) {
        this.cinco = cinco;
    }
}
