package com.cucinestudios.mastertoolsmaestros.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Nianya on 16/5/18.
 */

public class DatoChildItem  {

    String nombre;
    String total;
    Boolean last;

    public DatoChildItem(String nombre, String total, Boolean last) {
        this.nombre = nombre;
        this.total = total;
        this.last = last;
    }

    public Boolean getLast() {
        return last;
    }

    public void setLast(Boolean last) {
        this.last = last;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

}
