package com.cucinestudios.mastertoolsmaestros.Models;

/**
 * Created by nianya on 22/08/17.
 */

public class CrearAsistenciaItem {
    String id;
    int estado;

    public CrearAsistenciaItem(String id, int estado) {
        this.id = id;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
}
