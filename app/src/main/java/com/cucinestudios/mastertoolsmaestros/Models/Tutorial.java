package com.cucinestudios.mastertoolsmaestros.Models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nianya on 15/08/17.
 */

public class Tutorial extends RealmObject {
    @PrimaryKey
    String id;

    public String getId(){
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
