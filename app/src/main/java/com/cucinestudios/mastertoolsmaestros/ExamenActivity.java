package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Adapters.ExamenSectionAdapter;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.ExamenChildItem;
import com.cucinestudios.mastertoolsmaestros.Models.ExamenParentItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;

import cz.msebera.android.httpclient.Header;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.realm.Realm;

//import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

/**
 * Created by jesusnieves on 8/3/17.
 */
public class ExamenActivity extends AppCompatActivity {

    RecyclerView lista;
    RecyclerView.LayoutManager manager;
    Activity activity;
    ArrayList<ExamenChildItem> examenChildItems;
    SectionedRecyclerViewAdapter examenItemAdapter;
    FrameLayout play;
    Config config;
    Realm realm;
    TrailingCircularDotsLoader loading;
    String id;
    String idBimestre;
    String idGrupo;
    String url = "https://www.youtube.com/watch?v=_tNNvy7A9pI";
    boolean first = false;
    TextView semExamenes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.examenes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        play = (FrameLayout) toolbar.findViewById(R.id.examen_play);
        semExamenes = findViewById(R.id.semaforoExamen);
        toolbar.setTitle("Examenes");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config = new Config();

        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        lista = (RecyclerView) findViewById(R.id.examen_lista);
        examenItemAdapter = new SectionedRecyclerViewAdapter();
        lista.setHasFixedSize(true);
        manager = new LinearLayoutManager(activity);
        lista.setLayoutManager(manager);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
            idBimestre = bundle.getString("idBimestre");
            idGrupo =  bundle.getString("idGrupo");
        }

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,VideoActivity.class);
                intent.putExtra("url",url);
                startActivity(intent);
            }
        });

        getLista();
    }

    public void getLista(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        client.setTimeout(300000);
        String url = config.getUrl()+config.getExamenes();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
                first = true;

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try{
                    JSONObject respuesta = new JSONObject(new String(response));
                    double acumPromedio = 0;
                    double totalExamenes = 0, promExamenes = 0;
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray examenes = data.getJSONArray("examenes");
                        for(int i = 0; i<examenes.length(); i++) {

                            examenChildItems = new ArrayList<>();
                            JSONArray temas = examenes.getJSONObject(i).getJSONArray("temas");

                            String titulo = examenes.getJSONObject(i).getString("titulo");
                            String fecha = examenes.getJSONObject(i).getString("fechaEntrega");
                            String des = examenes.getJSONObject(i).getString("tipo");

                            DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                            DateTime temp = df.withOffsetParsed().parseDateTime(fecha);
                            DateTimeFormatter df2 = DateTimeFormat.forPattern("yyyy-MM-dd");
                            Log.i("examen","examen"+examenes.getJSONObject(i));
                            double promedio = 0;
                            if (!examenes.getJSONObject(i).isNull("promediototal")) {
                                promedio = round(examenes.getJSONObject(i).getDouble("promediototal"), 1);
                            }
                            for (int e = 0; e<temas.length(); e++ ) {

                                //examenChildItems = new ArrayList<>();

                                String nombreTema = temas.getJSONObject(e).getString("Nombre");
                                String calificacion = "null";
                                if (!temas.getJSONObject(e).isNull("Calificacion")) {
                                    calificacion = temas.getJSONObject(e).getString("Calificacion");
                                }
                                examenChildItems.add(new ExamenChildItem(temas.getJSONObject(e).getString("IDTema"),nombreTema,calificacion+"",false,""));

                            }
                            acumPromedio = acumPromedio + promedio;

                            
                            if(promedio<5) {
                                examenChildItems.add(new ExamenChildItem(examenes.getJSONObject(i).getString("idExamen"), "Total", "5 ("+promedio+")", true, ""));
                            }else if (promedio>10){
                                examenChildItems.add(new ExamenChildItem(examenes.getJSONObject(i).getString("idExamen"), "Total", "10 ("+promedio+")", true, ""));
                            }else{
                                examenChildItems.add(new ExamenChildItem(examenes.getJSONObject(i).getString("idExamen"), "Total", promedio+"", true, ""));
                            }
                            ExamenParentItem parentItem = new ExamenParentItem(examenes.getJSONObject(i).getString("idExamen"),titulo,temp.toString(df2),des,id,idGrupo);
                            examenItemAdapter.addSection(new ExamenSectionAdapter(activity,parentItem,examenChildItems));

                        }
                        totalExamenes = examenes.length();
                        promExamenes = (acumPromedio/totalExamenes)*10;
                        if(promExamenes<=60){
                            semExamenes.setBackgroundResource(R.drawable.semaforo_rojo);
                        }else if(promExamenes<90){
                            semExamenes.setBackgroundResource(R.drawable.semaforo_amarillo);
                        }else{
                            semExamenes.setBackgroundResource(R.drawable.semaforo_verde);
                        }
                        lista.setAdapter(examenItemAdapter);

                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                    first = true;

                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                first = true;

                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }

    public void getListaRefresh(){
        examenItemAdapter.removeAllSections();
        examenItemAdapter.notifyDataSetChanged();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        client.setTimeout(300000);
        String url = config.getUrl()+config.getExamenes();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try{
                    JSONObject respuesta = new JSONObject(new String(response));
                    double acumPromedio = 0;
                    double totalExamenes = 0, promExamenes = 0;
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray examenes = data.getJSONArray("examenes");
                        for(int i = 0; i<examenes.length(); i++) {
                            examenChildItems = new ArrayList<>();
                            JSONArray temas = examenes.getJSONObject(i).getJSONArray("temas");

                            String titulo = examenes.getJSONObject(i).getString("titulo");
                            String fecha = examenes.getJSONObject(i).getString("fechaEntrega");
                            String des = examenes.getJSONObject(i).getString("tipo");
                            double promedio = round(examenes.getJSONObject(i).getDouble("promediototal"),1);
                            DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                            DateTime temp = df.withOffsetParsed().parseDateTime(fecha);
                            DateTimeFormatter df2 = DateTimeFormat.forPattern("yyyy-MM-dd");


                            for (int e = 0; e<temas.length(); e++ ) {

                                //examenChildItems = new ArrayList<>();

                                String nombreTema = temas.getJSONObject(e).getString("Nombre");
                                String calificacion = "null";
                                if (!temas.getJSONObject(e).isNull("Calificacion")) {
                                    calificacion = temas.getJSONObject(e).getString("Calificacion");
                                }
                                examenChildItems.add(new ExamenChildItem(temas.getJSONObject(e).getString("IDTema"),nombreTema,calificacion+"",false,""));

                            }

                            if(promedio<5) {
                                examenChildItems.add(new ExamenChildItem(examenes.getJSONObject(i).getString("idExamen"), "Total", "5 ("+promedio+")", true, ""));
                            }else if (promedio>10){
                                examenChildItems.add(new ExamenChildItem(examenes.getJSONObject(i).getString("idExamen"), "Total", "10 ("+promedio+")", true, ""));
                            }else{
                                examenChildItems.add(new ExamenChildItem(examenes.getJSONObject(i).getString("idExamen"), "Total", promedio+"", true, ""));
                            }
                            ExamenParentItem parentItem = new ExamenParentItem(examenes.getJSONObject(i).getString("idExamen"),titulo,temp.toString(df2),des,id,idGrupo);
                            examenItemAdapter.addSection(new ExamenSectionAdapter(activity,parentItem,examenChildItems));
                        }
                        totalExamenes = examenes.length();
                        promExamenes = (acumPromedio/totalExamenes)*10;
                        if(promExamenes<=60){
                            semExamenes.setBackgroundResource(R.drawable.semaforo_rojo);
                        }else if(promExamenes<90){
                            semExamenes.setBackgroundResource(R.drawable.semaforo_amarillo);
                        }else{
                            semExamenes.setBackgroundResource(R.drawable.semaforo_verde);
                        }
                        examenItemAdapter.notifyDataSetChanged();

                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {

                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(examenItemAdapter != null && first ){
            getListaRefresh();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.alumnos, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

}

