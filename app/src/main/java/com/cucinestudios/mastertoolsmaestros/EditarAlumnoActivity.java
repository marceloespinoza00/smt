package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.GenericoItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import fr.ganfra.materialspinner.MaterialSpinner;
import io.realm.Realm;

/**
 * Created by niniparra on 6/3/17.
 */
public class EditarAlumnoActivity extends AppCompatActivity
{
    EditText nombre, apellido,apellidoM, curp;
    TextInputLayout lnombre, lapellido,lapellidoM, lcurp;
    MaterialSpinner nec;
    Config config;
    Activity activity;
    ArrayAdapter<GenericoItem> necAdapter;
    ArrayList<GenericoItem> necItems;
    Button guardar,cancelar;
    int es_nec = -1;
    String id;
    String idGrupo;
    String[] apellidos;
    TrailingCircularDotsLoader loading;
    Realm realm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nuevo_alumno);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Editar Alumno");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config = new Config();
        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        nombre = (EditText) findViewById(R.id.nuevo_alumno_nombre);
        apellido = (EditText) findViewById(R.id.nuevo_alumno_apellido);
        apellidoM = (EditText) findViewById(R.id.nuevo_alumno_apellido_materno);
        curp = (EditText) findViewById(R.id.nuevo_alumno_curp);
        lnombre = (TextInputLayout) findViewById(R.id.nuevo_alumno_layout_nombre);
        lapellido = (TextInputLayout) findViewById(R.id.nuevo_alumno_layout_apellido);
        lapellidoM = (TextInputLayout) findViewById(R.id.nuevo_alumno_layout_apellido_materno);
        lcurp = (TextInputLayout) findViewById(R.id.nuevo_alumno_layout_curp);
        nec = (MaterialSpinner) findViewById(R.id.nuevo_alumno_nec);
        guardar = (Button) findViewById(R.id.nuevo_alumno_guardar);
        cancelar = (Button) findViewById(R.id.nuevo_alumno_cancelar);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
            idGrupo = bundle.getString("idGrupo");
        }

        agregarNec();

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });

        getDetalle();
    }

    public  void getDetalle(){
        // nec.setText(necAdapter.getItem(posnec).getName());

        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        String url = config.getUrl()+config.getAlumno();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONObject alumno = data.getJSONObject("alumno");

                        String isnec = "No";
                        if (alumno.getBoolean("EsUSAER")){
                            isnec = "Si";
                        }

                        int posnec = getPosNEC(isnec);

                        nec.setSelection(posnec);

                        agregarNec();

                        nombre.setText(alumno.getString("Nombre"));
                        apellido.setText(alumno.getString("ApellidoPaterno"));
                        apellidoM.setText(alumno.getString("ApellidoMaterno"));
                        curp.setText(alumno.getString("Curp"));

                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });

    }

    public int getPosNEC(String codigo){
        int pos = 0;
        for (int i=0; i<necItems.size();i++){
            if (necItems.get(i).getName().matches(codigo)){
                pos = i;
            }
        }
        return pos;
    }

    private void submitForm() {
        if (!validateApellido()) {
            return;
        }
        if (!validateApellidoM()) {
            return;
        }
        if (!validateCurp()) {
            return;
        }
        if (!validateNEC()) {
            return;
        }
        if (!validateNombre()) {
            return;
        }

        setUpdateAlumno();

    }

    public void setUpdateAlumno(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("group",idGrupo);
        Log.e("alumno",id);
        Log.e("grupo",idGrupo);
        params.put("nombre",nombre.getText().toString());
        params.put("lastname",apellido.getText().toString());
        params.put("midlename", apellidoM.getText().toString());
        params.put("curp",curp.getText().toString());
        params.put("esnee",es_nec);
        String url = config.getUrl()+config.getUpdateAlumno();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.put(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                        finish();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }

    private boolean validateNombre() {
        if (nombre.getText().toString().trim().isEmpty()) {
            lnombre.setError("No puede estar vacio");
            requestFocus(nombre);
            return false;
        } else {
            lnombre.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateCurp() {
        if (curp.getText().toString().trim().isEmpty()) {
            lcurp.setError("No puede estar vacio");
            requestFocus(curp);
            return false;
        } else {
            lcurp.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateApellido() {
        if (apellido.getText().toString().trim().isEmpty()) {
            lapellido.setError("No puede estar vacio");
            requestFocus(apellido);
            return false;
        } else {
            lapellido.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateApellidoM() {
        if (apellidoM.getText().toString().trim().isEmpty()) {
            lapellidoM.setError("No puede estar vacio");
            requestFocus(apellidoM);
            return false;
        } else {
            lapellidoM.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validateNEC() {
        if (es_nec == -1) {
            nec.setError("No puede estar vacio");
            return false;
        }

        return true;
    }

    public void agregarNec(){
        necItems = new ArrayList<>();
        necItems.add(new GenericoItem(1,"Si"));
        necItems.add(new GenericoItem(0,"No"));
        necAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_dropdown_item_1line, necItems);
        nec.setAdapter(necAdapter);
        nec.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //es_nec = "";
                if (i > -1) {
                    es_nec = necItems.get(i).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

