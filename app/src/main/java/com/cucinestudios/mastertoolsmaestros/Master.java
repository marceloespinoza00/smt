package com.cucinestudios.mastertoolsmaestros;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by nini on 15/8/17.
 */

public class Master  extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .migration(new Migration())// Migration to run instead of throwing an exception
                .build();
        Realm.setDefaultConfiguration(config);
    }
}
