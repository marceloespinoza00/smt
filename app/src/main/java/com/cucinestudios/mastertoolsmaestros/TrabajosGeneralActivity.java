package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Adapters.TrabajoGeneralAdapter;
import com.cucinestudios.mastertoolsmaestros.Models.AsistenciaGeneralItem;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

/**
 * Created by Nianya on 12/6/18.
 */

public class TrabajosGeneralActivity extends AppCompatActivity {

    FloatingActionButton crear, asistencia, trabajo;
    RecyclerView lista;
    RecyclerView.LayoutManager manager;
    Activity activity;
    ArrayList<AsistenciaGeneralItem> alumnosItems;
    TrabajoGeneralAdapter alumnosAdapter;
    Config config;
    Realm realm;
    TrailingCircularDotsLoader loading;
    String id;
    String idTrabajo;
    int bimestre;
    FrameLayout play;
    String idbimestre;
    FloatingActionMenu menu;
    Button listo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trabajos_general);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        play = (FrameLayout) toolbar.findViewById(R.id.alumnos_play);
        toolbar.setTitle("Trabajos General");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config = new Config();
        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        menu = (FloatingActionMenu) findViewById(R.id.menu);
        crear = (FloatingActionButton) findViewById(R.id.alumnos_agregar);
        asistencia = (FloatingActionButton) findViewById(R.id.alumnos_asistencia);
        trabajo = (FloatingActionButton) findViewById(R.id.alumnos_trabajos);
        lista = (RecyclerView) findViewById(R.id.alumnos_lista);
        listo = (Button) findViewById(R.id.asistencia_general_listo);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
            bimestre = bundle.getInt("bimestre");
            idbimestre = bundle.getString("idBimestre");
            idTrabajo = bundle.getString("trabajo");
            Log.e("id",id);
            Log.e("bimestre",bimestre+"");
        }
        lista.setHasFixedSize(true);
        manager = new LinearLayoutManager(activity);
        lista.setLayoutManager(manager);
        listo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity,"Guardado Exitosamente",Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        getLista();
    }

    public void getLista(){
        alumnosItems = new ArrayList<>();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("grupo",id);
        params.put("bimestre",bimestre);
        String url = config.getUrl()+config.getAlumnos();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1) {
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray alumnos = data.getJSONArray("alumnos");
                        Log.e("al",alumnos.length()+"");
                        for (int i = 0; i<alumnos.length(); i++){
                            String name = alumnos.getJSONObject(i).getString("ApellidoPaterno")+" "+alumnos.getJSONObject(i).getString("ApellidoMaterno")+" "+alumnos.getJSONObject(i).getString("Nombre");
                            alumnosItems.add(new AsistenciaGeneralItem(alumnos.getJSONObject(i).getString("IDAlumno"),name,1,""));
                        }
                        alumnosAdapter = new TrabajoGeneralAdapter(activity,alumnosItems);
                        lista.setAdapter(alumnosAdapter);
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }
    public void actualizarEstado(String idAlumno, final int estado, final int pos){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",idAlumno);
        params.put("trabajo",idTrabajo);
        params.put("estado",estado);
        String url = config.getUrl()+config.getUpdateTrabajoAlumno();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.put(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1) {
                        alumnosItems.get(pos).setTipo(estado);
                        alumnosAdapter.notifyDataSetChanged();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.asistencia_general, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();
                break;
            case R.id.action_play:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
