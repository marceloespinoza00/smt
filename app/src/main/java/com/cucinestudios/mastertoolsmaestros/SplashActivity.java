package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.Tutorial;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.felipecsl.gifimageview.library.GifImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by nini on 15/8/17.
 */

public class SplashActivity extends AppCompatActivity {
    Activity activity;
    Config config;
    Realm realm;
    ImageView logo;
    GifImageView gif;
    Handler handler;
    TrailingCircularDotsLoader loading;
    Tutorial tutorial;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        activity = this;
        config = new Config();
        realm = Realm.getDefaultInstance();
        tutorial = realm.where(Tutorial.class).findFirst();
        gif = (GifImageView) findViewById(R.id.gif);
        logo = (ImageView) findViewById(R.id.logo);
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        if (tutorial != null){
            gif.setVisibility(View.GONE);
            logo.setVisibility(View.VISIBLE);
            loading.setVisibility(View.GONE);
            RealmResults<User> users = realm.where(User.class).findAll();
            if (users.size() > 0) {
                Intent intent = new Intent(activity, MainActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(activity, LoginActivity.class);
                startActivity(intent);
            }
            finish();
        }else {
            realm.beginTransaction();
            tutorial = realm.createObject(Tutorial.class, "1");
            realm.commitTransaction();
            gif.setVisibility(View.VISIBLE);
            logo.setVisibility(View.GONE);
            loading.setVisibility(View.GONE);
            AsyncHttpClient client = new AsyncHttpClient();
            client.get("http://app.mastertools.mx/Content/Images/Logo%20SecMasterTools.gif", new AsyncHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    loading.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    loading.setVisibility(View.GONE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            RealmResults<User> users = realm.where(User.class).findAll();
                            if (users.size() > 0) {
                                Intent intent = new Intent(activity, MainActivity.class);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(activity, LoginActivity.class);
                                startActivity(intent);
                            }
                            finish();
                        }
                    }, 5100);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    gif.setBytes(responseBody);
                    gif.startAnimation();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                }
            });
        }

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

}

