package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Adapters.AlumnosAdapter;
import com.cucinestudios.mastertoolsmaestros.Models.AlumnosItem;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

/**
 * Created by niniparra on 2/3/17.
 */
public class AlumnosActivity extends AppCompatActivity {

    FloatingActionButton crear,asistencia,trabajo;
    RecyclerView lista;
    RecyclerView.LayoutManager manager;
    Activity activity;
    ArrayList<AlumnosItem> alumnosItems;
    AlumnosAdapter alumnosAdapter;
    FrameLayout play;
    Config config;
    Realm realm;
    TrailingCircularDotsLoader loading;
    String id;
    int bimestre;
    String url = "https://www.youtube.com/watch?v=GpdMJOKgbqQ";
    SwipeRefreshLayout refreshLayout;
    FloatingActionMenu menu;
    String idBimestre;
    boolean first = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alumnos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        play = (FrameLayout) toolbar.findViewById(R.id.alumnos_play);
        toolbar.setTitle("Alumnos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config = new Config();
        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.alumnos_refresh);
        crear = (FloatingActionButton) findViewById(R.id.alumnos_agregar);
        menu = (FloatingActionMenu) findViewById(R.id.menu);
        asistencia = (FloatingActionButton) findViewById(R.id.alumnos_asistencia);
        trabajo = (FloatingActionButton) findViewById(R.id.alumnos_trabajos);
        lista = (RecyclerView) findViewById(R.id.alumnos_lista);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
            bimestre = bundle.getInt("bimestre");
        }

        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu.close(true);
                Intent intent = new Intent(activity,NuevoAlumnoActivity.class);
                intent.putExtra("id",id);
                startActivity(intent);
            }
        });


        asistencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu.close(true);
                Intent intent = new Intent(activity,AsistenciaGeneralActivity.class);
                intent.putExtra("id",id);
                intent.putExtra("bimestre",bimestre);
                startActivity(intent);
            }
        });


        trabajo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu.close(true);
                Intent intent = new Intent(activity,NuevoTrabajoGeneralActivity.class);
                intent.putExtra("idBimestre",idBimestre);
                intent.putExtra("bimestre",bimestre);
                intent.putExtra("idGrupo",id);
                startActivity(intent);
            }
        });



        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent = new Intent(activity,VideoActivity.class);
                intent.putExtra("url",url);
                startActivity(intent);
            }
        });


        refreshLayout.setColorSchemeColors(ContextCompat.getColor(activity,R.color.azul_oscuro),ContextCompat.getColor(activity,R.color.azul),ContextCompat.getColor(activity,R.color.naranja),ContextCompat.getColor(activity,R.color.verde),ContextCompat.getColor(activity,R.color.rojo));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListaRefresh();
            }
        });

        lista.setHasFixedSize(true);
        manager = new LinearLayoutManager(activity);
        lista.setLayoutManager(manager);
        getLista();
    }


    public void getListaRefresh(){
        alumnosItems.clear();
        refreshLayout.setRefreshing(true);
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("grupo",id);
        params.put("bimestre",bimestre);
        String url = config.getUrl()+config.getAlumnos();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url,params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray alumnos = data.getJSONArray("alumnos");
                        Log.e("al",alumnos.length()+"");
                        for (int i = 0; i<alumnos.length(); i++){
                            String name = alumnos.getJSONObject(i).getString("ApellidoPaterno")+" "+alumnos.getJSONObject(i).getString("ApellidoMaterno")+" "+alumnos.getJSONObject(i).getString("Nombre");
                            alumnosItems.add(new AlumnosItem(alumnos.getJSONObject(i).getString("IDAlumno"),name));
                        }
                        alumnosAdapter.notifyDataSetChanged();
                        refreshLayout.setRefreshing(false);

                    }else {
                        refreshLayout.setRefreshing(false);
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    refreshLayout.setRefreshing(false);
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                refreshLayout.setRefreshing(false);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });


    }


    public void getLista(){
        alumnosItems = new ArrayList<>();
        alumnosAdapter = new AlumnosAdapter(activity,alumnosItems);
        lista.setAdapter(alumnosAdapter);
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("grupo",id);
        params.put("bimestre",bimestre);
        String url = config.getUrl()+config.getAlumnos();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url,params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
                first = true;

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray alumnos = data.getJSONArray("alumnos");
                        idBimestre = respuesta.getString("IDBimestre");
                        final String idGrupo = respuesta.getString("IDGrupo");
                        Log.e("al",alumnos.length()+"");
                        for (int i = 0; i<alumnos.length(); i++){
                            String name = alumnos.getJSONObject(i).getString("ApellidoPaterno")+" "+alumnos.getJSONObject(i).getString("ApellidoMaterno")+" "+alumnos.getJSONObject(i).getString("Nombre");
                            alumnosItems.add(new AlumnosItem(alumnos.getJSONObject(i).getString("IDAlumno"),name));
                        }
                        Log.e("al",alumnosItems.size()+"");
                        alumnosAdapter.notifyDataSetChanged();
                        lista.addOnItemTouchListener(new RecyclerItemClick(activity, lista, new RecyclerItemClick.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Intent intent = new Intent(activity,AlumnoActivity.class);
                                intent.putExtra("id",alumnosItems.get(position).getId());
                                intent.putExtra("bimestre",bimestre);
                                intent.putExtra("idBimestre",idBimestre);
                                intent.putExtra("idGrupo",idGrupo);
                                startActivity(intent);
                            }

                            @Override
                            public void onItemLongClick(View view, int position) {

                            }
                        }));
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                    first = true;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                first = true;
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (alumnosAdapter != null && alumnosItems != null && first) {
            getListaRefresh();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.alumnos, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();

                break;
            case R.id.action_play:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
