package com.cucinestudios.mastertoolsmaestros.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.R;

/**
 * Created by Nianya on 16/5/18.
 */

public class DatoParentHolder extends RecyclerView.ViewHolder {

    public TextView bimestre;
    public ImageView expand;
    public LinearLayout padre;

    public DatoParentHolder(View itemView) {

        super(itemView);

        bimestre = (TextView) itemView.findViewById(R.id.dato_item_parent_bimestre);
        expand = (ImageView) itemView.findViewById(R.id.dato_item_parent_expand);
        padre = (LinearLayout) itemView.findViewById(R.id.padre);
    }

}
