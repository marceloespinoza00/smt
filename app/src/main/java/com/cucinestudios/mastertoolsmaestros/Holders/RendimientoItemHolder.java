package com.cucinestudios.mastertoolsmaestros.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.R;

/**
 * Created by Nianya on 16/5/18.
 */

public class RendimientoItemHolder extends RecyclerView.ViewHolder {

    public TextView childNombre;
    public ImageView childTotal;
    public LinearLayout hijo;

    public RendimientoItemHolder(View itemView) {

        super(itemView);

        childNombre = (TextView) itemView.findViewById(R.id.item_nombre);
        childTotal = (ImageView) itemView.findViewById(R.id.item_leyenda);
        hijo = (LinearLayout) itemView.findViewById(R.id.hijo);
    }

}
