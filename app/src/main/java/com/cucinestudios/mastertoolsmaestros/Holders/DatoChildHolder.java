package com.cucinestudios.mastertoolsmaestros.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.R;

/**
 * Created by Nianya on 16/5/18.
 */

public class DatoChildHolder extends RecyclerView.ViewHolder  {

    public TextView childNombre;
    public TextView childTotal;
    public TextView linea;
    public LinearLayout hijo;

    public DatoChildHolder(View itemView) {

        super(itemView);

        childNombre = (TextView) itemView.findViewById(R.id.dato_item_child_nombre);
        childTotal = (TextView) itemView.findViewById(R.id.dato_item_child_total);
        linea = (TextView) itemView.findViewById(R.id.dato_item_child_linea);
        hijo = (LinearLayout) itemView.findViewById(R.id.hijo);
    }

}
