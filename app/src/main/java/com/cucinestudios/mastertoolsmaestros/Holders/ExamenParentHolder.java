package com.cucinestudios.mastertoolsmaestros.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.R;

//import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

/**
 * Created by jesusnieves on 8/3/17.
 */
public class ExamenParentHolder extends RecyclerView.ViewHolder {

    public TextView parcial;
    public TextView fecha;
    public TextView des;
    public ImageView expand;
    public TextView tema_titulo;
    public LinearLayout padre;

    public ExamenParentHolder(View itemView) {

        super(itemView);

        parcial = (TextView) itemView.findViewById(R.id.examen_item_parent_parcial);
        fecha = (TextView) itemView.findViewById(R.id.examen_item_parent_fecha);
        des = (TextView) itemView.findViewById(R.id.examen_item_parent_des);
        tema_titulo = (TextView) itemView.findViewById(R.id.examen_item_parent_tema);
        padre = (LinearLayout) itemView.findViewById(R.id.padre);
        expand = (ImageView) itemView.findViewById(R.id.examen_item_parent_expand);
    }

}
