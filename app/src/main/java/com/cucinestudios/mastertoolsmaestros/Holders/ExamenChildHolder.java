package com.cucinestudios.mastertoolsmaestros.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.R;

//import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;

/**
 * Created by jesusnieves on 8/3/17.
 */
public class ExamenChildHolder extends RecyclerView.ViewHolder {

    public TextView childTema;
    public TextView childCalif;
    public TextView linea;
    public LinearLayout hijo;

    public ExamenChildHolder(View itemView) {

        super(itemView);

        childTema = (TextView) itemView.findViewById(R.id.examen_item_child_tema);
        childCalif = (TextView) itemView.findViewById(R.id.examen_item_child_cali);
        linea = (TextView) itemView.findViewById(R.id.examen_item_child_linea);
        hijo = (LinearLayout) itemView.findViewById(R.id.hijo);
    }

}
