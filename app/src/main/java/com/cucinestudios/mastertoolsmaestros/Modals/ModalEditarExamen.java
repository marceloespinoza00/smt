package com.cucinestudios.mastertoolsmaestros.Modals;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cucinestudios.mastertoolsmaestros.InputFilterMinMax;
import com.cucinestudios.mastertoolsmaestros.Interface.ModalEditarExamenInterface;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.cucinestudios.mastertoolsmaestros.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

/**
 * Created by jesusnieves on 14/3/17.
 */
public class ModalEditarExamen extends DialogFragment {
    EditText texto;
    EditText cali;
    Button listo;
    ModalEditarExamenInterface listener;
    Activity activity;
    Config config;
    Realm realm;
    String id;
    String idExamen;
    String idGrupo;

    public static ModalEditarExamen newInstance() {
        ModalEditarExamen f = new ModalEditarExamen();
        return f;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setStyle(STYLE_NO_TITLE,0);

        return super.onCreateDialog(savedInstanceState);

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.modal_editar_examen, container, false);
        texto = (EditText) view.findViewById(R.id.modal_editar_examen_tema);
        cali = (EditText) view.findViewById(R.id.modal_editar_examen_cali);
        listo = (Button) view.findViewById(R.id.modal_editar_examen_listo);
        cali.setFilters(new InputFilter[]{ new InputFilterMinMax("0", "10")});
        activity = getActivity();
        config = new Config();
        realm = Realm.getDefaultInstance();

        Bundle args = getArguments();
        id = args.getString("id");
        idExamen = args.getString("idExamen");
        idGrupo = args.getString("idGrupo");

        listo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
//                listener.onFinishEditDialog(texto.getText().toString(),cali.getText().toString());
//                dismiss();
            }
        });

        return view;
    }

    private void submitForm(){
        if(!validateTema()){
            return;
        }
        if(!validateCalif()){
            return;
        }
        setTema();
    }

    public void setTema(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("examen",idExamen);
        params.put("grupo",idGrupo);
        params.put("nombre",texto.getText().toString());
        params.put("calificacion", Integer.parseInt(cali.getText().toString()));
        String url = config.getUrl()+config.getTemaExamen();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.post(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                        listener.onFinishEditDialog("","");
                        dismiss();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }


    private boolean validateTema(){
        if(texto.getText().toString().trim().isEmpty()){
            Toast.makeText(activity,"Los datos no pueden estar vacíos",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateCalif(){
        if(cali.getText().toString().trim().isEmpty()){
            Toast.makeText(activity,"Los datos no pueden estar vacíos",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (ModalEditarExamenInterface) context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString() + "no esta implentando la interface");
        }
    }

}
