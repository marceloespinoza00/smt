package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

/**
 * Created by jesusnieves on 7/4/17.
 */
public class LoginActivity  extends AppCompatActivity {
    Activity activity;
    TextInputLayout lusuario;
    TextInputLayout lpass;
    EditText usuario;
    EditText pass;
    Button iniciar;
    Button registro;
    Config config;
    Realm realm;
    TrailingCircularDotsLoader loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.login);
        activity = this;
        config = new Config();
        lusuario = (TextInputLayout) findViewById(R.id.login_layout_usuario);
        lpass = (TextInputLayout) findViewById(R.id.login_layout_pass);
        pass = (EditText) findViewById(R.id.login_pass);
        usuario = (EditText) findViewById(R.id.login_usuario);
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        realm = Realm.getDefaultInstance();
        iniciar = (Button) findViewById(R.id.login_iniciar);
        registro = (Button) findViewById(R.id.login_registrar);

        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("click","login");
                submitForm();
            }
        });
        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://app.mastertools.mx/Account/Register"));
                startActivity(browserIntent);
            }
        });
    }

    private void submitForm() {
        if (!validateCorreo()) {
            return;
        }
        if (!validatePass()) {
            return;
        }
        login();
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean validateCorreo() {
        String email = usuario.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            lusuario.setError("Correo electronico invalido");
            requestFocus(lusuario);
            return false;
        }
        return true;
    }
    private boolean validatePass() {
        String email = pass.getText().toString().trim();
        if (email.isEmpty()) {
            lpass.setError("La contraseña no puede estar vacia");
            requestFocus(lpass);
            return false;
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void login(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("email",usuario.getText().toString());
        String url = config.getUrl()+config.getLogin();
        Log.e("url",url);
        client.post(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONObject user = data.getJSONObject("user");
                        realm.beginTransaction();
                        String token = data.getString("token");
                        User item = realm.createObject(User.class, user.getString("Id"));
                        item.setEmail(user.getString("Email"));
                        item.setToken(token);
                        realm.commitTransaction();
                        Intent intent = new Intent(activity,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

}
