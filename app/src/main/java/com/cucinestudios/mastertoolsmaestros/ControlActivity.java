package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Adapters.ControlAdapter;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.ControlItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

/**
 * Created by niniparra on 26/4/17.
 */
public class ControlActivity extends AppCompatActivity {

    RecyclerView lista;
    RecyclerView.LayoutManager manager;
    Activity activity;
    ArrayList<ControlItem> controlItems;
    ControlAdapter controlAdapter;
    CoordinatorLayout coordinatorLayout;
    ControlItem item;
    EditText alumno;
    ImageView rendimiento;
    FrameLayout play;
    Button datos;
    Config config;
    Realm realm;
    TrailingCircularDotsLoader loading;
    String id;
    String grupo;
    int bimestre;
    String url = "https://www.youtube.com/watch?v=HS6a1GrD1aA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.control);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Control");
        setSupportActionBar(toolbar);
        play = (FrameLayout) toolbar.findViewById(R.id.control_play);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config = new Config();
        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        lista = (RecyclerView) findViewById(R.id.control_lista);
        rendimiento = (ImageView) findViewById(R.id.control_rendimiento);
        datos = (Button) findViewById(R.id.control_datos);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator);
        lista.setHasFixedSize(true);
        manager = new LinearLayoutManager(activity);
        lista.setLayoutManager(manager);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
            bimestre = bundle.getInt("bimestre");
            grupo = bundle.getString("grupo");
        }
        rendimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(activity,RendimientoActivity.class);
                intent.putExtra("id",id);
                intent.putExtra("bimestre",bimestre);
                intent.putExtra("grupo",grupo);
                startActivity(intent);
            }
        });
        datos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(activity,DatosActivity.class);
                intent.putExtra("id",id);
                startActivity(intent);
            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,VideoActivity.class);
                intent.putExtra("url",url);
                startActivity(intent);
            }
        });

        getLista();
    }

    public void getLista(){
        controlItems = new ArrayList<>();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",bimestre);
        params.put("grupo",grupo);
        String url = config.getUrl()+config.getListControl();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray instrumentos = data.getJSONArray("instrumentos");
                        Log.e("al",instrumentos.length()+"");
                        for (int i = 0; i<instrumentos.length(); i++){
                            String tema = instrumentos.getJSONObject(i).getString("Nombre");
                            double valor = instrumentos.getJSONObject(i).getDouble("total");
                            double pon = instrumentos.getJSONObject(i).getDouble("ponderacion");
                            controlItems.add(new ControlItem(instrumentos.getJSONObject(i).getString("Nombre"),tema,String.format("%.1f", valor),String.format("%.1f", pon)));
                        }
                        controlAdapter = new ControlAdapter(controlItems);
                        controlAdapter.setHasStableIds(true);
                        controlAdapter.setOnItemClickListener(new ControlAdapter.ListItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                Log.i("CLICKK ITEMM","CLICKK"+position);
                                controlAdapter.notifyItemChanged(position);
                            }
                        });
                        lista.setAdapter(controlAdapter);
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.alumnos, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

}



