package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Adapters.InstrumentoSectionAdapter;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.ExamenChildItem;
import com.cucinestudios.mastertoolsmaestros.Models.ExamenParentItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.realm.Realm;


/**
 * Created by jesusnieves on 17/3/17.
 */
public class InstrumentosActivity extends AppCompatActivity {

    RecyclerView lista;
    RecyclerView.LayoutManager manager;
    Activity activity;
    ArrayList<ExamenChildItem> instrumentoChildItems;
    SectionedRecyclerViewAdapter instrumentoAdapter;
    FrameLayout play;
    Config config;
    Realm realm;
    TrailingCircularDotsLoader loading;
    String id;
    String idBimestre;
    String url = "https://www.youtube.com/watch?v=m8tYrT9tsmg";
    boolean first = false;
    TextView semInstrumentos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.instrumentos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        play = (FrameLayout) toolbar.findViewById(R.id.instrumento_play);
        semInstrumentos = findViewById(R.id.semaforoInstrumento);
        toolbar.setTitle("Instrumentos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config= new Config();
        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        lista = (RecyclerView) findViewById(R.id.instrumento_lista);
        lista.setHasFixedSize(true);
        manager = new LinearLayoutManager(activity);
        lista.setLayoutManager(manager);
        instrumentoAdapter = new SectionedRecyclerViewAdapter();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
            idBimestre = bundle.getString("idBimestre");
        }

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,VideoActivity.class);
                intent.putExtra("url",url);
                startActivity(intent);
            }
        });

        getLista();
    }

    public void getLista(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        final RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        client.setTimeout(300000);
        String url = config.getUrl()+config.getInstrumentos();
        Log.e("url",url+params.toString());
        Log.i("bearerrr","bearerr"+user.getToken());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
                first = true;
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try{
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray instrumentos = data.getJSONArray("instrumentos");
                        double sumPromedio = 0;
                        Log.e("al",instrumentos.length()+"");
                        for(int i = 0; i<instrumentos.length(); i++){

                            JSONArray temas = instrumentos.getJSONObject(i).getJSONArray("temas");
                            String titulo = instrumentos.getJSONObject(i).getString("nombre");
                            String descripcion = instrumentos.getJSONObject(i).getString("descripcion");
                            String fecha = instrumentos.getJSONObject(i).getString("fechaEntrega");
                            double promedio = round(instrumentos.getJSONObject(i).getDouble("promedio"),1);

                            DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                            DateTime temp = df.withOffsetParsed().parseDateTime(fecha);
                            DateTimeFormatter df2 = DateTimeFormat.forPattern("yyyy-MM-dd");

                            for (int e = 0; e<temas.length(); e++){

                                instrumentoChildItems = new ArrayList<>();

                                String nombreAspecto1 = temas.getJSONObject(e).getString("nombreAspecto1");
                                String nombreAspecto2 = temas.getJSONObject(e).getString("nombreAspecto2");
                                String nombreAspecto3 = temas.getJSONObject(e).getString("nombreAspecto3");
                                String nombreAspecto4 = temas.getJSONObject(e).getString("nombreAspecto4");
                                String nombreAspecto5 = temas.getJSONObject(e).getString("nombreAspecto5");

                                String aspecto1 = temas.getJSONObject(e).getString("Aspecto1");
                                String aspecto2 = temas.getJSONObject(e).getString("Aspecto2");
                                String aspecto3 = temas.getJSONObject(e).getString("Aspecto3");
                                String aspecto4 = temas.getJSONObject(e).getString("Aspecto4");
                                String aspecto5 = temas.getJSONObject(e).getString("Aspecto5");

                                if(!temas.getJSONObject(e).isNull("nombreAspecto1")){
                                    instrumentoChildItems.add(new ExamenChildItem("1",nombreAspecto1,aspecto1,false,""));
                                }

                                if(!temas.getJSONObject(e).isNull("nombreAspecto2")){
                                    instrumentoChildItems.add(new ExamenChildItem("2",nombreAspecto2,aspecto2,false,""));
                                }

                                if(!temas.getJSONObject(e).isNull("nombreAspecto3")){
                                    instrumentoChildItems.add(new ExamenChildItem("3",nombreAspecto3,aspecto3,false,""));
                                }

                                if(!temas.getJSONObject(e).isNull("nombreAspecto4")){
                                    instrumentoChildItems.add(new ExamenChildItem("4",nombreAspecto4,aspecto4,false,""));
                                }

                                if(!temas.getJSONObject(e).isNull("nombreAspecto5")){
                                    instrumentoChildItems.add(new ExamenChildItem("5",nombreAspecto5,aspecto5,false,""));
                                }
                                if(promedio<5) {
                                    sumPromedio = sumPromedio + 5;
                                    instrumentoChildItems.add(new ExamenChildItem("6", "Total", "5 ("+promedio+")", true, ""));
                                }else if (promedio>10){
                                    sumPromedio = sumPromedio + 10;
                                    instrumentoChildItems.add(new ExamenChildItem("6", "Total", "10 ("+promedio+")", true, ""));
                                }else{
                                    sumPromedio = sumPromedio + promedio;
                                    instrumentoChildItems.add(new ExamenChildItem("6", "Total", promedio+"", true, ""));
                                }
                            }
                            double totalInstrumentos = instrumentos.length();
                            double promInstrumentos = (sumPromedio/totalInstrumentos)*10;
                            int enteroInstrumentos = (int) promInstrumentos;
                            if(enteroInstrumentos<=60){
                                semInstrumentos.setBackgroundResource(R.drawable.semaforo_rojo);
                            }else if(enteroInstrumentos<90){
                                semInstrumentos.setBackgroundResource(R.drawable.semaforo_amarillo);
                            }else{
                                semInstrumentos.setBackgroundResource(R.drawable.semaforo_verde);
                            }
                            ExamenParentItem parentItem = new ExamenParentItem(instrumentos.getJSONObject(i).getString("idPortafolio"),titulo,temp.toString(df2),descripcion,instrumentos.getJSONObject(i).getString("idPortafolioAlumno"), "");
                            instrumentoAdapter.addSection(new InstrumentoSectionAdapter(activity,parentItem,instrumentoChildItems));
                        }

                        lista.setAdapter(instrumentoAdapter);

                    }else {
                        first = true;

                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("einstrumentos",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                first = true;
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });

    }


    public void getListaRefresh(){
        instrumentoAdapter.removeAllSections();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        client.setTimeout(300000);
        String url = config.getUrl()+config.getInstrumentos();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try{
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray instrumentos = data.getJSONArray("instrumentos");
                        double sumPromedio = 0;

                        for(int i = 0; i<instrumentos.length(); i++){

                            JSONArray temas = instrumentos.getJSONObject(i).getJSONArray("temas");

                            String titulo = instrumentos.getJSONObject(i).getString("nombre");
                            String descripcion = instrumentos.getJSONObject(i).getString("descripcion");
                            String fecha = instrumentos.getJSONObject(i).getString("fechaEntrega");
                            double promedio = round(instrumentos.getJSONObject(i).getDouble("promedio"),1);

                            DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                            DateTime temp = df.withOffsetParsed().parseDateTime(fecha);
                            DateTimeFormatter df2 = DateTimeFormat.forPattern("yyyy-MM-dd");

                            for (int e = 0; e<temas.length(); e++){

                                instrumentoChildItems = new ArrayList<>();

                                String nombreAspecto1 = temas.getJSONObject(e).getString("nombreAspecto1");
                                String nombreAspecto2 = temas.getJSONObject(e).getString("nombreAspecto2");
                                String nombreAspecto3 = temas.getJSONObject(e).getString("nombreAspecto3");
                                String nombreAspecto4 = temas.getJSONObject(e).getString("nombreAspecto4");
                                String nombreAspecto5 = temas.getJSONObject(e).getString("nombreAspecto5");

                                String aspecto1 = temas.getJSONObject(e).getString("Aspecto1");
                                String aspecto2 = temas.getJSONObject(e).getString("Aspecto2");
                                String aspecto3 = temas.getJSONObject(e).getString("Aspecto3");
                                String aspecto4 = temas.getJSONObject(e).getString("Aspecto4");
                                String aspecto5 = temas.getJSONObject(e).getString("Aspecto5");

                                if(!temas.getJSONObject(e).isNull("nombreAspecto1")){
                                    instrumentoChildItems.add(new ExamenChildItem("1",nombreAspecto1,aspecto1,false,""));
                                }

                                if(!temas.getJSONObject(e).isNull("nombreAspecto2")){
                                    instrumentoChildItems.add(new ExamenChildItem("2",nombreAspecto2,aspecto2,false,""));
                                }

                                if(!temas.getJSONObject(e).isNull("nombreAspecto3")){
                                    instrumentoChildItems.add(new ExamenChildItem("3",nombreAspecto3,aspecto3,false,""));
                                }

                                if(!temas.getJSONObject(e).isNull("nombreAspecto4")){
                                    instrumentoChildItems.add(new ExamenChildItem("4",nombreAspecto4,aspecto4,false,""));
                                }

                                if(!temas.getJSONObject(e).isNull("nombreAspecto5")){
                                    instrumentoChildItems.add(new ExamenChildItem("5",nombreAspecto5,aspecto5,false,""));
                                }

                                if(promedio<5) {
                                    sumPromedio = sumPromedio + 5;
                                    instrumentoChildItems.add(new ExamenChildItem("6", "Total", "5 ("+promedio+")", true, ""));
                                }else if (promedio>10){
                                    sumPromedio = sumPromedio + 10;
                                    instrumentoChildItems.add(new ExamenChildItem("6", "Total", "10 ("+promedio+")", true, ""));
                                }else{
                                    sumPromedio = sumPromedio + promedio;
                                    instrumentoChildItems.add(new ExamenChildItem("6", "Total", promedio+"", true, ""));
                                }
                            }
                            double totalInstrumentos = instrumentos.length();
                            double promInstrumentos = (sumPromedio/totalInstrumentos)*10;
                            int enteroInstrumentos = (int) promInstrumentos;
                            if(enteroInstrumentos<=60){
                                semInstrumentos.setBackgroundResource(R.drawable.semaforo_rojo);
                            }else if(enteroInstrumentos<90){
                                semInstrumentos.setBackgroundResource(R.drawable.semaforo_amarillo);
                            }else{
                                semInstrumentos.setBackgroundResource(R.drawable.semaforo_verde);
                            }
                            ExamenParentItem parentItem = new ExamenParentItem(instrumentos.getJSONObject(i).getString("idPortafolio"),titulo,temp.toString(df2),"",instrumentos.getJSONObject(i).getString("idPortafolioAlumno"), descripcion);
                            instrumentoAdapter.addSection(new InstrumentoSectionAdapter(activity,parentItem,instrumentoChildItems));
                        }

                        instrumentoAdapter.notifyDataSetChanged();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });

    }
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(instrumentoAdapter != null && first){
            getListaRefresh();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.alumnos, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

}


