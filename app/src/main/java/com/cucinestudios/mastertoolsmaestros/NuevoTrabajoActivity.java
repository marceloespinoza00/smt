package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.TimeZone;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import ru.slybeaver.slycalendarview.SlyCalendarDialog;

/**
 * Created by jesusnieves on 20/3/17.
 */
public class NuevoTrabajoActivity extends AppCompatActivity implements SlyCalendarDialog.Callback{

    TextView dia;
    Activity activity;
    Button listo;
    String date;
    EditText nombre, tipo;
    TextInputLayout lnombre, ltipo;
    Config config;
    Realm realm;
    String idBimestre;
    String idGrupo;

    private static final String FRAG_TAG_DATE_PICKER = "fragment_date_picker_name";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nuevo_trabajo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Nuevo Trabajo");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config = new Config();
        realm = Realm.getDefaultInstance();
        dia = (TextView) findViewById(R.id.nuevo_trabajo_dia);
        nombre = (EditText) findViewById(R.id.nuevo_trabajo_nombre);
        tipo = (EditText) findViewById(R.id.nuevo_trabajo_tipo);
        lnombre = (TextInputLayout) findViewById(R.id.nuevo_trabajo_layout_nombre);
        ltipo = (TextInputLayout) findViewById(R.id.nuevo_trabajo_layout_tipo);
        listo = (Button) findViewById(R.id.nuevo_trabajo_listo);
        listo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            idBimestre = bundle.getString("idBimestre");
            idGrupo = bundle.getString("idGrupo");
        }
        dia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SlyCalendarDialog()
                        .setSingle(true)
                        .setBackgroundColor(ContextCompat.getColor(activity, R.color.blanco))
                        .setHeaderTextColor(ContextCompat.getColor(activity, R.color.blanco))
                        .setHeaderColor(ContextCompat.getColor(activity, R.color.colorPrimary))
                        .setSelectedTextColor(ContextCompat.getColor(activity, R.color.blanco))
                        .setTextColor(ContextCompat.getColor(activity, R.color.negro))
                        .setCallback(NuevoTrabajoActivity.this)
                        .show(getSupportFragmentManager(), FRAG_TAG_DATE_PICKER);
            }
        });

    }

    private void submitForm() {
        if (!validateNombre()) {
            return;
        }
        if (!validateTipo()) {
            return;
        }
        if (!validateFecha()) {
            return;
        }
//        Intent intent = new Intent(activity,TrabajoGeneralActivity.class);
//        startActivity(intent);
//        finish();
        setTrabajo();
    }

    public void setTrabajo(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("grupo",idGrupo);
        params.put("bimestre",idBimestre);
        params.put("nombre",nombre.getText().toString());
        params.put("tipo",tipo.getText().toString());
        params.put("date",date);
//        params.put("nombre",nombre.getText().toString());
        String url = config.getUrl()+config.getCreateTrabajo();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.post(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        Toast.makeText(activity, "Trabajo creado exitosamente",Toast.LENGTH_SHORT).show();
                        finish();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }

    private boolean validateNombre() {
        if (nombre.getText().toString().trim().isEmpty()) {
            lnombre.setError("No puede estar vacio");
            requestFocus(nombre);
            return false;
        } else {
            lnombre.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateTipo() {
        if (tipo.getText().toString().trim().isEmpty()) {
            ltipo.setError("No puede estar vacio");
            requestFocus(tipo);
            return false;
        } else {
            ltipo.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateFecha() {
        if (date.trim().isEmpty()) {
            dia.setError("No puede estar vacio");
            return false;
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.alumnos, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCancelled() {

    }

    @Override
    public void onDataSelected(Calendar firstDate, Calendar secondDate, int hours, int minutes) {
        if (firstDate != null) {
            TimeZone tz = firstDate.getTimeZone();
            DateTimeZone jodaTz = DateTimeZone.forID(tz.getID());
            DateTime dateTime = new DateTime(firstDate.getTimeInMillis(), jodaTz);
            dia.setText(dateTime.getDayOfMonth() + "/" + (dateTime.getMonthOfYear()) + "/" + dateTime.getYear());
            date = dateTime.getYear() + "/" + (dateTime.getMonthOfYear() + 1) + "/" + dateTime.getDayOfMonth();
            Log.e("e",date);
        }
    }
}


