package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.DecimalDigitsInputFilter;
import com.cucinestudios.mastertoolsmaestros.Models.ControlItem;
import com.cucinestudios.mastertoolsmaestros.R;
import com.cucinestudios.mastertoolsmaestros.RecyclerItemClick;
import com.mikhaellopez.hfrecyclerview.HFRecyclerView;

import java.util.ArrayList;

/**
 * Created by niniparra on 26/4/17.
 */
public class ControlAdapter extends HFRecyclerView<ControlItem> {

    ArrayList<ControlItem> controlItems;
    private Context context;
    private  ListItemClickListener mOnClickListener;


    public interface ListItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(ListItemClickListener mOnClickListener){
        this.mOnClickListener=mOnClickListener;
    }

    public ControlAdapter(ArrayList<ControlItem> controlItems) {
        super(controlItems, true, true);
        this.controlItems = controlItems;
    }
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            final ControlItem item = getItem(position);
            itemViewHolder.tema.setText(item.getTema());
            itemViewHolder.cali.setText(item.getCali());
            itemViewHolder.pon.setText(item.getPor());
            if(itemViewHolder.pon.equals(100)){
                itemViewHolder.pon.setTextColor(ContextCompat.getColor(context, R.color.verde));
            }
            itemViewHolder.pon.setFilters(new InputFilter[]{ new DecimalDigitsInputFilter(10,1)});
            itemViewHolder.pon.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            item.setPor(itemViewHolder.pon.getText().toString());
                            notifyItemChanged(getItemCount()-1);
                        }
                    });
                }
            });
            itemViewHolder.pon.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE){
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                item.setPor(itemViewHolder.pon.getText().toString());
                                notifyItemChanged(getItemCount()-1);
                            }
                        });
                    }
                    return false;
                }
            });
        } else if (holder instanceof HeaderViewHolder) {

        } else if (holder instanceof FooterViewHolder) {
            FooterViewHolder itemViewHolder = (FooterViewHolder) holder;
            itemViewHolder.cali.setText(promedio());
            itemViewHolder.por.setText(porcentaje());
            itemViewHolder.tema.setText("Total");
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    //region Override Get ViewHolder
    @Override
    protected RecyclerView.ViewHolder getItemView(LayoutInflater inflater, ViewGroup parent) {
        return new ItemViewHolder(inflater.inflate(R.layout.control_item, parent, false), mOnClickListener);
    }

    @Override
    protected RecyclerView.ViewHolder getHeaderView(LayoutInflater inflater, ViewGroup parent) {
        return new HeaderViewHolder(inflater.inflate(R.layout.control_header, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getFooterView(LayoutInflater inflater, ViewGroup parent) {
        return new FooterViewHolder(inflater.inflate(R.layout.control_footer, parent, false));
    }
    //endregion

    //region ViewHolder Header and Footer
    class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView tema;
        TextView cali;
        EditText pon;
        public ItemViewHolder(View itemView, final ListItemClickListener listener) {
            super(itemView);
            context = itemView.getContext();
            tema = (TextView)itemView.findViewById(R.id.control_item_tema);
            cali = (TextView)itemView.findViewById(R.id.control_item_cali);
            pon = (EditText) itemView.findViewById(R.id.control_item_pon);
        }


    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        TextView tema;
        TextView cali;
        TextView por;
        public FooterViewHolder(View itemView) {
            super(itemView);
            tema = (TextView)itemView.findViewById(R.id.control_footer_tema);
            cali = (TextView)itemView.findViewById(R.id.control_footer_cali);
            por = (TextView)itemView.findViewById(R.id.control_footer_pon);

        }
    }


    public String promedio(){
        String prome = "";
        double prom = 0.0;
        double sumpromedio = 0.0;
        for (int i= 0; i<controlItems.size(); i++){
            Log.i("promee","promee"+i+"  "+controlItems.get(i).getTema());
            sumpromedio += Double.parseDouble(controlItems.get(i).getCali());
        }
        prom = sumpromedio/getItemCount();
        prome = String.valueOf(Math.round(prom));
        return prome;
    }

    public String porcentaje(){
        String prome = "";
        double prom = 0.0;
        double sumpromedio = 0.0;
        for (int i= 0; i<controlItems.size(); i++){
            sumpromedio += Double.parseDouble(controlItems.get(i).getPor());
        }
        prom = sumpromedio;
        prome = String.valueOf(Math.round(prom));
        return prome;
    }
}
