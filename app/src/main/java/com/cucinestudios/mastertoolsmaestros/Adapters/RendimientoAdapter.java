package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cucinestudios.mastertoolsmaestros.Holders.RendimientoItemHolder;
import com.cucinestudios.mastertoolsmaestros.Models.RendimientoItem;
import com.cucinestudios.mastertoolsmaestros.R;

import java.util.ArrayList;

/**
 * Created by jesusnieves on 27/4/17.
 */
public class RendimientoAdapter extends RecyclerView.Adapter<RendimientoItemHolder> {

    private LayoutInflater mInflater;
    Activity activity;
    ArrayList<RendimientoItem> items;


    public RendimientoAdapter(Activity activity, ArrayList<RendimientoItem> items) {
        this.activity = activity;
        this.items = items;
    }


    @Override
    public RendimientoItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rendimiento_list, parent, false);
        return new RendimientoItemHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RendimientoItemHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        RendimientoItem ditem = items.get(position);
        holder.childNombre.setText(ditem.getNombre());
        holder.childTotal.getDrawable().setColorFilter(ditem.getColor(), PorterDuff.Mode.SRC );

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

}
