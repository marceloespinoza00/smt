package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.Models.AsistenciaGeneralItem;
import com.cucinestudios.mastertoolsmaestros.Models.GenericoItem;
import com.cucinestudios.mastertoolsmaestros.R;

import java.util.ArrayList;

/**
 * Created by jesusnieves on 19/5/17.
 */
public class AsistenciaGeneralAdapter extends RecyclerView.Adapter<AsistenciaGeneralAdapter.ViewHolder> {
    ArrayList<AsistenciaGeneralItem> items;
    Activity activity;
    ArrayList<GenericoItem> asisItems;
    ArrayAdapter<GenericoItem> asisAdapter;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView nombre;
        public Spinner tipo;
        public ViewHolder(View v) {
            super(v);
            nombre = (TextView) v.findViewById(R.id.asistencia_general_item_nombre);
            tipo = (Spinner) v.findViewById(R.id.asistencia_general_item_tipo);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AsistenciaGeneralAdapter(Activity activity, ArrayList<AsistenciaGeneralItem> items) {
        this.items = items;
        this.activity = activity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AsistenciaGeneralAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        // create a new view
        LayoutInflater inflater = (LayoutInflater) LayoutInflater.from(activity);
        View rowView = inflater.inflate(R.layout.asistencia_general_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(rowView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final AsistenciaGeneralItem ditem = items.get(position);
        asisItems = new ArrayList<>();
        asisItems.add(new GenericoItem(1,"Falta"));
        asisItems.add(new GenericoItem(2,"Asistencia"));
        asisItems.add(new GenericoItem(3,"Retardo"));
        asisItems.add(new GenericoItem(4,"Suspension"));
        asisItems.add(new GenericoItem(5,"Justificacion"));
        asisAdapter = new ArrayAdapter<>(activity,
        android.R.layout.simple_dropdown_item_1line, asisItems);
        holder.tipo.setAdapter(asisAdapter);
        holder.tipo.setSelection(ditem.getTipo());
        holder.tipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    ditem.setTipo(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        holder.nombre.setText(ditem.getNombre());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }
}

