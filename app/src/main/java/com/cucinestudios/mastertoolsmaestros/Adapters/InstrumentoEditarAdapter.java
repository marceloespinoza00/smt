package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cucinestudios.mastertoolsmaestros.InputFilterMinMax;
import com.cucinestudios.mastertoolsmaestros.InstrumentoEditarActivty;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.ExamenChildItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.cucinestudios.mastertoolsmaestros.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

/**
 * Created by jesusnieves on 8/3/17.
 */
public class InstrumentoEditarAdapter extends RecyclerView.Adapter<InstrumentoEditarAdapter.ViewHolder> {
    ArrayList<ExamenChildItem> items;
    Activity activity;
    Config config;
    Realm realm;
    String id;
    String idAlumno;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView nombre;
        public EditText cali;
        public TextView linea;
        public ViewHolder(View v) {
            super(v);
            nombre = (TextView) v.findViewById(R.id.examen_editar_item_tema);
            cali = (EditText) v.findViewById(R.id.examen_editar_item_cali);
            linea = (TextView) v.findViewById(R.id.examen_editar_item_linea);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public InstrumentoEditarAdapter(Activity activity, String id, String idAlumno, ArrayList<ExamenChildItem> items) {
        this.items = items;
        this.activity = activity;
        this.id = id;
        this.idAlumno = idAlumno;
        realm = Realm.getDefaultInstance();
        config = new Config();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public InstrumentoEditarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        LayoutInflater inflater = (LayoutInflater) LayoutInflater.from(activity);
        View rowView = inflater.inflate(R.layout.examen_editar_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(rowView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final ExamenChildItem ditem = items.get(position);
        final ExamenChildItem last = items.get(getItemCount()-1);
        holder.nombre.setText(ditem.getTema());
        holder.cali.setText(ditem.getCalif());
        if (position == getItemCount()-1){
            holder.linea.setVisibility(View.VISIBLE);
            holder.nombre.setEnabled(false);
            holder.cali.setEnabled(false);
        }else{
            holder.linea.setVisibility(View.GONE);
            holder.nombre.setEnabled(true);
            holder.cali.setEnabled(true);

        }
        holder.nombre.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE){
                    ditem.setTema(holder.nombre.getText().toString());
                    notifyDataSetChanged();
                }
                return false;
            }
        });
        holder.cali.setFilters(new InputFilter[]{ new InputFilterMinMax("0", "10")});
        holder.cali.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE){
                    if(!ditem.getCalif().equals(holder.cali.getText().toString())){
                        ditem.setCalif(holder.cali.getText().toString());
                        last.setCalif(promedio());
                        editarNota(id, idAlumno, ditem.getAspecto(), holder.cali.getText().toString());
                    }
                }
                return false;
            }
        });

        holder.cali.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if(!ditem.getCalif().equals(holder.cali.getText().toString())){
                        Log.e("cali","prueba instrumento");
                        Log.e("focus-false",hasFocus+"");
                        ditem.setCalif(holder.cali.getText().toString());
                        last.setCalif(promedio());
                        editarNota(id, idAlumno, ditem.getAspecto(), holder.cali.getText().toString());
                    }
                }
            }
        });
    }


    public void editarNota(String id, String idAlumno, String aspecto, String nota){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("portafolio",id);
        params.put("alumno",idAlumno);
        params.put("aspecto",aspecto);
        params.put("calificacion",nota);
        String url = config.getUrl()+config.getUpdateInstrumentos();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.put(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                ((InstrumentoEditarActivty)activity).loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                ((InstrumentoEditarActivty)activity).loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    notifyDataSetChanged();
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }


    public String promedio(){
        String prome = "";
        double prom = 0.0;
        double sumpromedio = 0.0;
        for (int i= 0; i<items.size(); i++){
            sumpromedio += Double.parseDouble(items.get(i).getCalif());
        }
        prom = sumpromedio/getItemCount();
        prome = String.valueOf(Math.round(prom));
        return prome;
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }
}