package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.EditarGrupoActivity;
import com.cucinestudios.mastertoolsmaestros.Models.GruposItem;
import com.cucinestudios.mastertoolsmaestros.R;

import java.util.ArrayList;

/**
 * Created by niniparra on 23/2/17.
 */
public class GruposAdapter extends RecyclerView.Adapter<GruposAdapter.ViewHolder> {
    ArrayList<GruposItem> items;
    Activity activity;
    GruposNumerosAdapter adapter;
    String idGrupo;

public static class ViewHolder extends RecyclerView.ViewHolder {
    // each data item is just a string in this case
    public TextView seccion;
    public TextView escuela;
    public TextView nombre;
    public FrameLayout conf;
    public RecyclerView numeros;
    public RecyclerView.LayoutManager manager;
    public LinearLayout color;
    public ImageView taller;
    public ViewHolder(View v) {
        super(v);
        seccion = (TextView) v.findViewById(R.id.lista_grupos_seccion);
        escuela = (TextView) v.findViewById(R.id.lista_grupos_escuela);
        nombre = (TextView) v.findViewById(R.id.lista_grupos_nombre);
        conf = (FrameLayout) v.findViewById(R.id.lista_grupos_config);
        numeros = (RecyclerView) v.findViewById(R.id.lista_grupos_numeros);
        color = (LinearLayout) v.findViewById(R.id.lista_grupos_color);
        taller = (ImageView) v.findViewById(R.id.lista_grupos_taller);

    }
}

    // Provide a suitable constructor (depends on the kind of dataset)
    public GruposAdapter(Activity activity, ArrayList<GruposItem> items) {
        this.items = items;
        this.activity = activity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public GruposAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        // create a new view
        LayoutInflater inflater = (LayoutInflater) LayoutInflater.from(activity);
        View rowView = inflater.inflate(R.layout.lista_grupos, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(rowView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element.
        final GruposItem ditem = items.get(position);
        holder.nombre.setText(ditem.getNombre());
        holder.escuela.setText(ditem.getEscuela());
        holder.seccion.setText(ditem.getSeccion());
        holder.color.setBackgroundColor(Color.parseColor(ditem.getColor()));
        holder.numeros.setHasFixedSize(true);
        holder.manager = new LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false);
        holder.numeros.setLayoutManager(holder.manager);

        // specify an adapter (see also next example)
        adapter = new GruposNumerosAdapter(activity,ditem.getNumeros());
        holder.numeros.setAdapter(adapter);
        holder.conf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(activity,EditarGrupoActivity.class);
                intent.putExtra("idGrupo",ditem.getId());
                activity.startActivity(intent);
            }
        });

        if (ditem.getColor().toLowerCase().equals("#ebebeb")){
            holder.nombre.setTextColor(ContextCompat.getColor(activity,R.color.negro));
            holder.escuela.setTextColor(ContextCompat.getColor(activity,R.color.negro));
            holder.seccion.setTextColor(ContextCompat.getColor(activity,R.color.negro));
            holder.taller.setVisibility(View.GONE);
            if (ditem.isTaller()){
                holder.taller.setVisibility(View.VISIBLE);
                holder.taller.setColorFilter(ContextCompat.getColor(activity, R.color.negro));
            }
        }else{
            holder.nombre.setTextColor(ContextCompat.getColor(activity,R.color.blanco));
            holder.escuela.setTextColor(ContextCompat.getColor(activity,R.color.blanco));
            holder.seccion.setTextColor(ContextCompat.getColor(activity,R.color.blanco));
            holder.taller.setVisibility(View.GONE);
            if (ditem.isTaller()){
                holder.taller.setVisibility(View.VISIBLE);
                holder.taller.setColorFilter(ContextCompat.getColor(activity, R.color.blanco));

            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }
}

