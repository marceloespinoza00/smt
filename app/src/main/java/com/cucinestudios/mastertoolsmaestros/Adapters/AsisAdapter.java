package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.AsistenciasActivity;
import com.cucinestudios.mastertoolsmaestros.Models.AlumnosItem;
import com.cucinestudios.mastertoolsmaestros.Models.AsistenciaGeneralItem;
import com.cucinestudios.mastertoolsmaestros.Models.GenericoItem;
import com.cucinestudios.mastertoolsmaestros.R;

import java.util.ArrayList;

/**
 * Created by niniparra on 2/3/17.
 */
public class AsisAdapter extends RecyclerView.Adapter<AsisAdapter.ViewHolder> {
    ArrayList<AsistenciaGeneralItem> items;
    Activity activity;
    ArrayList<GenericoItem> asisItems;
    ArrayAdapter<GenericoItem> asisAdapter;
    boolean first = false;

    public static class ViewHolder extends RecyclerView.ViewHolder {
     public TextView nombre;
        Spinner tipo;
        public ViewHolder(View v) {
        super(v);
            nombre = (TextView) v.findViewById(R.id.asistencias_item_fecha);
            tipo = (Spinner) v.findViewById(R.id.asistencia_item_tipo);

        }
    }

    public AsisAdapter(Activity activity, ArrayList<AsistenciaGeneralItem> items) {
        this.items = items;
        this.activity = activity;
    }

    @Override
    public AsisAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) LayoutInflater.from(activity);
        View rowView = inflater.inflate(R.layout.asistencias_item, parent, false);
        ViewHolder vh = new ViewHolder(rowView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final AsistenciaGeneralItem ditem = items.get(position);
        holder.nombre.setText(ditem.getNombre());
        asisItems = new ArrayList<>();
        asisItems.add(new GenericoItem(1,"Falta"));
        asisItems.add(new GenericoItem(2,"Asistencia"));
        asisItems.add(new GenericoItem(3,"Retardo"));
        asisItems.add(new GenericoItem(4,"Suspension"));
        asisItems.add(new GenericoItem(5,"Justificacion"));
        asisAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_dropdown_item_1line, asisItems);
        holder.tipo.setAdapter(asisAdapter);
        holder.tipo.setSelection(ditem.getTipo());
        holder.tipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != ditem.getTipo() && first){
                    ((AsistenciasActivity) activity).changeAsistencia(ditem.getId(),ditem.getAlumno(),i);
                }
                first = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }
}

