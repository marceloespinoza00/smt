package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.Models.AsistenciaGeneralItem;
import com.cucinestudios.mastertoolsmaestros.Models.TrabajoSpinnerItem;
import com.cucinestudios.mastertoolsmaestros.R;
import com.cucinestudios.mastertoolsmaestros.TrabajosGeneralActivity;

import java.util.ArrayList;

/**
 * Created by jesusnieves on 24/5/17.
 */
public class TrabajoGeneralAdapter extends RecyclerView.Adapter<TrabajoGeneralAdapter.ViewHolder> {
    ArrayList<AsistenciaGeneralItem> items;
    Activity activity;
    ArrayList<TrabajoSpinnerItem> spinnerItems;
    TrabajoSpinnerAdapter adapter;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView nombre;
        public Spinner tipo;
        public ViewHolder(View v) {
            super(v);
            nombre = (TextView) v.findViewById(R.id.asistencia_general_item_nombre);
            tipo = (Spinner) v.findViewById(R.id.asistencia_general_item_tipo);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public TrabajoGeneralAdapter(Activity activity, ArrayList<AsistenciaGeneralItem> items) {
        this.items = items;
        this.activity = activity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TrabajoGeneralAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                  int viewType) {
        // create a new view
        LayoutInflater inflater = (LayoutInflater) LayoutInflater.from(activity);
        View rowView = inflater.inflate(R.layout.asistencia_general_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(rowView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final AsistenciaGeneralItem ditem = items.get(position);
        final boolean[] first = {false};
        spinnerItems = new ArrayList<>();
        spinnerItems.add(new TrabajoSpinnerItem(1,"No Entregado",0));
        spinnerItems.add(new TrabajoSpinnerItem(2,"Entregado",1));
        spinnerItems.add(new TrabajoSpinnerItem(3,"Medio Entregado",2));
        adapter = new TrabajoSpinnerAdapter(activity, R.layout.trabajo_spinner, spinnerItems);
        holder.tipo.setAdapter(adapter);
        holder.tipo.setSelection(ditem.getTipo());
        holder.tipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("getT", ditem.getTipo()+"");
                Log.e("getT2", spinnerItems.get(i).getType()+"");
                if (spinnerItems.get(i).getType() != ditem.getTipo() && first[0]) {
                    ((TrabajosGeneralActivity) activity).actualizarEstado(ditem.getId(), spinnerItems.get(i).getType(), position);
                }
                first[0] = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        holder.nombre.setText(ditem.getNombre());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }
}

