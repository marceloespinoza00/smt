package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.AsistenciasActivity;
import com.cucinestudios.mastertoolsmaestros.Models.AsistenciaGeneralItem;
import com.cucinestudios.mastertoolsmaestros.Models.GenericoItem;
import com.cucinestudios.mastertoolsmaestros.R;

import java.util.ArrayList;

/**
 * Created by jesusnieves on 19/5/17.
 */
public class AsistenciaAdapter extends BaseAdapter {
    ArrayList<AsistenciaGeneralItem> items;
    Activity activity;
    ArrayList<GenericoItem> asisItems;
    ArrayAdapter<GenericoItem> asisAdapter;
    boolean first = false;

    @Override
    public int getCount() {
        return  items.size();
    }

    @Override
    public Object getItem(int i) {
        return  items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = (LayoutInflater) LayoutInflater.from(activity);
            v = vi.inflate(R.layout.asistencias_item, null);
        }
        TextView nombre = (TextView) v.findViewById(R.id.asistencias_item_fecha);
        Spinner tipo = (Spinner) v.findViewById(R.id.asistencia_item_tipo);
        final AsistenciaGeneralItem ditem = items.get(position);
        asisItems = new ArrayList<>();
        asisItems.add(new GenericoItem(1,"Falta"));
        asisItems.add(new GenericoItem(2,"Asistencia"));
        asisItems.add(new GenericoItem(3,"Retardo"));
        asisItems.add(new GenericoItem(4,"Suspension"));
        asisItems.add(new GenericoItem(5,"Justificacion"));
        asisAdapter = new ArrayAdapter<>(activity,
        android.R.layout.simple_dropdown_item_1line, asisItems);
        tipo.setAdapter(asisAdapter);
        tipo.setSelection(ditem.getTipo());
        tipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != ditem.getTipo() && first){
                    ((AsistenciasActivity) activity).changeAsistencia(ditem.getId(),ditem.getAlumno(),i);
                }
                first = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        nombre.setText(ditem.getNombre());

        return v;
    }


    // Provide a suitable constructor (depends on the kind of dataset)
    public AsistenciaAdapter(Activity activity, ArrayList<AsistenciaGeneralItem> items) {
        this.items = items;
        this.activity = activity;
    }

}

