package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.Models.TrabajoSpinnerItem;
import com.cucinestudios.mastertoolsmaestros.R;

import java.util.ArrayList;

/**
 * Created by jesusnieves on 19/5/17.
 */
public class TrabajoSpinnerAdapter extends ArrayAdapter<TrabajoSpinnerItem>{

    private Activity activity;
    private ArrayList data;
    TrabajoSpinnerItem tempValues=null;
    LayoutInflater inflater;

    /*************  CustomAdapter Constructor *****************/
    public TrabajoSpinnerAdapter(Activity activitySpinner, int textViewResourceId, ArrayList objects) {
        super(activitySpinner, textViewResourceId, objects);

        /********** Take passed values **********/
        activity = activitySpinner;
        data     = objects;

        /***********  Layout inflator to call external xml layout () **********************/
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.trabajo_spinner, parent, false);

        /***** Get each Model object from Arraylist ********/
        tempValues = null;
        tempValues = (TrabajoSpinnerItem) data.get(position);

        TextView tres = (TextView)row.findViewById(R.id.trabajo_spinner_tres);
        ImageView uno = (ImageView)row.findViewById(R.id.trabajo_spinner_uno);
        ImageView dos = (ImageView)row.findViewById(R.id.trabajo_spinner_dos);

        if(position==1){
            uno.setVisibility(View.VISIBLE);
            dos.setVisibility(View.GONE);
            tres.setVisibility(View.GONE);
        }
        else if(position==0){
            uno.setVisibility(View.GONE);
            dos.setVisibility(View.VISIBLE);
            tres.setVisibility(View.GONE);
        } else if(position==2){
            uno.setVisibility(View.GONE);
            dos.setVisibility(View.GONE);
            tres.setVisibility(View.VISIBLE);
        }

        return row;
    }
}