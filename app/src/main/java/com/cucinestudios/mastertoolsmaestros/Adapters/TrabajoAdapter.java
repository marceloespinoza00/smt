package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.Models.TrabajoItem;
import com.cucinestudios.mastertoolsmaestros.Models.TrabajoSpinnerItem;
import com.cucinestudios.mastertoolsmaestros.R;
import com.cucinestudios.mastertoolsmaestros.TrabajoActivity;
import com.mikhaellopez.hfrecyclerview.HFRecyclerView;

import java.util.ArrayList;


/**
 * Created by jesusnieves on 17/3/17.
 */
public class TrabajoAdapter extends HFRecyclerView<TrabajoItem> {
    ArrayList<TrabajoItem> trabajoItems;
    ArrayList<TrabajoSpinnerItem> spinnerItems;
    TrabajoSpinnerAdapter adapter;
    Activity activity;
    boolean first = false;

    public TrabajoAdapter(ArrayList<TrabajoItem> trabajoItem, Activity activity) {
        super(trabajoItem, true, false);
        this.trabajoItems = trabajoItem;
        this.activity = activity;
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            final TrabajoItem item = getItem(position);
            itemViewHolder.nombre.setText(item.getNombre());
            itemViewHolder.fecha.setText(item.getFecha());
            itemViewHolder.tipo.setText(item.getTipo());
            spinnerItems = new ArrayList<>();
            spinnerItems.add(new TrabajoSpinnerItem(1,"No Entregado",0));
            spinnerItems.add(new TrabajoSpinnerItem(2,"Entregado",1));
            spinnerItems.add(new TrabajoSpinnerItem(3,"Incompleto",2));
            adapter = new TrabajoSpinnerAdapter(activity, R.layout.trabajo_spinner, spinnerItems);
            // Set adapter to spinner
            itemViewHolder.estado.setAdapter(adapter);
            itemViewHolder.estado.setSelection(item.getEstado());
            itemViewHolder.estado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (spinnerItems.get(i).getType() != item.getEstado() && first) {
                       ((TrabajoActivity) activity).actualizarEstado(item.getId(), spinnerItems.get(i).getType(), position-1);
                    }
                    first = true;
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } else if (holder instanceof HeaderViewHolder) {
            int entregado = 0, no_entregados = 0,incompletos = 0;
            HeaderViewHolder itemViewHolder = (HeaderViewHolder) holder;
            for (int i = 0; i<trabajoItems.size();i++){
                if (trabajoItems.get(i).getEstado() == 1){
                    entregado++;
                }else{
                    if (trabajoItems.get(i).getEstado() == 2){
                        incompletos++;
                    }else{
                        no_entregados++;
                    }
                }
            }
            itemViewHolder.entregados.setText(entregado+"");
            itemViewHolder.no_entregados.setText(no_entregados+"");
            itemViewHolder.incompleto.setText(incompletos+"");


        } else if (holder instanceof FooterViewHolder) {

        }
    }
    //region Override Get ViewHolder
    @Override
    protected RecyclerView.ViewHolder getItemView(LayoutInflater inflater, ViewGroup parent) {
        return new ItemViewHolder(inflater.inflate(R.layout.trabajo_item, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getHeaderView(LayoutInflater inflater, ViewGroup parent) {
        return new HeaderViewHolder(inflater.inflate(R.layout.trabajo_footer, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getFooterView(LayoutInflater inflater, ViewGroup parent) {
        return null;
    }
    //endregion

    //region ViewHolder Header and Footer
    class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView nombre;
        TextView tipo;
        TextView fecha;
        Spinner estado;
        public ItemViewHolder(View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.trabajo_item_nombre);
            nombre.setHorizontallyScrolling(true);
            nombre.setMovementMethod(new ScrollingMovementMethod());
            tipo = itemView.findViewById(R.id.trabajo_item_des);
            tipo.setHorizontallyScrolling(true);
            tipo.setMovementMethod(new ScrollingMovementMethod());
            fecha = itemView.findViewById(R.id.trabajo_item_fecha);
            estado =  itemView.findViewById(R.id.trabajo_item_estado);
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView entregados;
        TextView no_entregados;
        TextView incompleto;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            entregados = (TextView)itemView.findViewById(R.id.trabajo_footer_entregados);
            no_entregados = (TextView)itemView.findViewById(R.id.trabajo_footer_no);
            incompleto = (TextView)itemView.findViewById(R.id.trabajo_footer_medio);
        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }

    public void updateList() {
        notifyDataSetChanged();
    }

}
