package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.cucinestudios.mastertoolsmaestros.Holders.DatoChildHolder;
import com.cucinestudios.mastertoolsmaestros.Holders.DatoParentHolder;
import com.cucinestudios.mastertoolsmaestros.Holders.ExamenChildHolder;
import com.cucinestudios.mastertoolsmaestros.Holders.ExamenParentHolder;
import com.cucinestudios.mastertoolsmaestros.Models.DatoChildItem;
import com.cucinestudios.mastertoolsmaestros.Models.DatoParentItem;
import com.cucinestudios.mastertoolsmaestros.R;

import java.util.ArrayList;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

/**
 * Created by jesusnieves on 27/4/17.
 */
public class DatoAdapter extends StatelessSection {
    Activity activity;
    DatoParentItem datoParentItem;
    ArrayList<DatoChildItem> datoChildItems;

    public DatoAdapter(Activity activity, DatoParentItem datoParentItem, ArrayList<DatoChildItem> datoChildItems) {
        super(SectionParameters.builder()
                .itemResourceId(R.layout.datos_item)
                .headerResourceId(R.layout.datos_header)
                .build());
        this.datoChildItems = datoChildItems;
        this.datoParentItem = datoParentItem;
        this.activity = activity;

    }

    @Override
    public int getContentItemsTotal() {
        return datoChildItems.size(); // number of items of this section
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new DatoChildHolder(view);
    }

    @Override
    public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        final DatoChildHolder itemHolder = (DatoChildHolder) holder;
        final DatoChildItem childItem = datoChildItems.get(position);
        itemHolder.childNombre.setText(childItem.getNombre());
        itemHolder.childTotal.setText(childItem.getTotal());
        if (datoChildItems.size() - 1 == position) {
            itemHolder.linea.setVisibility(View.VISIBLE);
        } else {
            itemHolder.linea.setVisibility(View.GONE);
        }

    }

    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
        return new DatoParentHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
        DatoParentHolder holderH = (DatoParentHolder) holder;
        holderH.bimestre.setText(datoParentItem.getBimestre());
    }
}
