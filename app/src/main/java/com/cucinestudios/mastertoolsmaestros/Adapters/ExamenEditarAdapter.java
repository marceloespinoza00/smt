package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cucinestudios.mastertoolsmaestros.ExamenEditarActivity;
import com.cucinestudios.mastertoolsmaestros.InputFilterMinMax;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.ExamenChildItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.cucinestudios.mastertoolsmaestros.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

/**
 * Created by jesusnieves on 8/3/17.
 */
public class ExamenEditarAdapter extends RecyclerView.Adapter<ExamenEditarAdapter.ViewHolder> {

    ArrayList<ExamenChildItem> items;
    Activity activity;
    Config config;
    Realm realm;
    String id;
    public static final int ITEM_TYPE_NORMAL = 0;
    public static final int ITEM_TYPE_LAST = 1;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView nombre;
        public EditText cali;
        public TextView linea;
        public ViewHolder(View v) {
            super(v);
            nombre = (TextView) v.findViewById(R.id.examen_editar_item_tema);
            cali = (EditText) v.findViewById(R.id.examen_editar_item_cali);
            linea = (TextView) v.findViewById(R.id.examen_editar_item_linea);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ExamenEditarAdapter(Activity activity, String id, ArrayList<ExamenChildItem> items) {
        this.items = items;
        this.activity = activity;
        this.id = id;
        realm = Realm.getDefaultInstance();
        config = new Config();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ExamenEditarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {

        if (viewType == ITEM_TYPE_NORMAL) {
            LayoutInflater inflater =  LayoutInflater.from(activity);
            View rowView = inflater.inflate(R.layout.examen_editar_item, parent, false);
            ViewHolder vh = new ViewHolder(rowView);
            return vh;
        }else{
            LayoutInflater inflater =  LayoutInflater.from(activity);
            View rowView = inflater.inflate(R.layout.examen_editar_item2, parent, false);
            ViewHolder vh = new ViewHolder(rowView);
            return vh;
        }

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final ExamenChildItem ditem = items.get(position);
        final ExamenChildItem last = items.get(getItemCount()-1);
        final int itemType = getItemViewType(position);
        holder.nombre.setText(ditem.getTema());
        holder.cali.setText(ditem.getCalif());
        if (itemType == ITEM_TYPE_NORMAL) {
            holder.linea.setVisibility(View.GONE);
            holder.nombre.setEnabled(true);
            holder.cali.setEnabled(true);
        holder.cali.setFilters(new InputFilter[]{ new InputFilterMinMax("0", "10")});
        holder.nombre.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE){
                    ditem.setTema(holder.nombre.getText().toString());
                    notifyDataSetChanged();
                }
                return false;
            }
        });

        holder.cali.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE){
                    if(!ditem.getCalif().equals(holder.cali.getText().toString())){
                        ditem.setCalif(holder.cali.getText().toString());
                        last.setCalif(promedio());
                        updateExamen(id, ditem.getId(), Integer.parseInt(holder.cali.getText().toString()));
                    }
                }
                return false;
            }
        });

        holder.cali.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if(!ditem.getCalif().equals(holder.cali.getText().toString())){
                        Log.e("cali","prueba examen");
                        Log.e("focus-false",hasFocus+"");
                        ditem.setCalif(holder.cali.getText().toString());
                        last.setCalif(promedio());
                        updateExamen(id, ditem.getId(), Integer.parseInt(holder.cali.getText().toString()));
                    }
                }
            }
        });
        }else{
            holder.linea.setVisibility(View.VISIBLE);
            holder.nombre.setEnabled(false);
            holder.cali.setEnabled(false);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position).getLast()) {
            return ITEM_TYPE_LAST;
        } else {
            return ITEM_TYPE_NORMAL;
        }
    }

    public void updateExamen(String id, String idTema, int calificacion){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("tema",idTema);
        params.put("calificacion",calificacion);
        String url = config.getUrl()+config.getUpdateExamen();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.put(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                ((ExamenEditarActivity)activity).loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                ((ExamenEditarActivity)activity).loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    notifyDataSetChanged();

                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }


    public String promedio(){
        String prome = "";
        double prom = 0.0;
        double sumpromedio = 0.0;
        for (int i= 0; i<items.size(); i++){
            sumpromedio += Double.parseDouble(items.get(i).getCalif());
        }
        prom = sumpromedio/getItemCount();
        prome = String.valueOf(Math.round(prom));
        return prome;
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }
}

