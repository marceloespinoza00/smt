package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.AlumnosActivity;
import com.cucinestudios.mastertoolsmaestros.Models.GruposNumerosItem;
import com.cucinestudios.mastertoolsmaestros.R;

import java.util.ArrayList;

/**
 * Created by niniparra on 23/2/17.
 */
public class GruposNumerosAdapter extends RecyclerView.Adapter<GruposNumerosAdapter.ViewHolder> {
    ArrayList<GruposNumerosItem> items;
    Activity activity;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView numero;
        public ViewHolder(View v) {
            super(v);
            numero = (TextView) v.findViewById(R.id.lista_grupos_numeros_circulo);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public GruposNumerosAdapter(Activity activity, ArrayList<GruposNumerosItem> items) {
        this.items = items;
        this.activity = activity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public GruposNumerosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        LayoutInflater inflater = (LayoutInflater) LayoutInflater.from(activity);
        View rowView = inflater.inflate(R.layout.lista_grupos_numeros, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(rowView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final GruposNumerosItem ditem = items.get(position);
        holder.numero.setText(ditem.getName());
        holder.numero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity,AlumnosActivity.class);
                intent.putExtra("id",ditem.getId_grupo());
                intent.putExtra("bimestre", position+1);
                activity.startActivity(intent);
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }
}


