package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.cucinestudios.mastertoolsmaestros.Holders.ExamenChildHolder;
import com.cucinestudios.mastertoolsmaestros.Holders.ExamenParentHolder;
import com.cucinestudios.mastertoolsmaestros.InstrumentoEditarActivty;
import com.cucinestudios.mastertoolsmaestros.Models.ExamenChildItem;
import com.cucinestudios.mastertoolsmaestros.Models.ExamenParentItem;
import com.cucinestudios.mastertoolsmaestros.R;

import java.util.ArrayList;
import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

/**
 * Created by jesusnieves on 17/3/17.
 */
public class InstrumentoSectionAdapter extends StatelessSection {
    Activity activity;
    ExamenParentItem examenParentItems;
    ArrayList<ExamenChildItem> examenChildItems;

    public InstrumentoSectionAdapter(Activity activity, ExamenParentItem examenParentItems, ArrayList<ExamenChildItem> examenChildItems) {
        super(SectionParameters.builder()
                .itemResourceId(R.layout.examen_item_child)
                .headerResourceId(R.layout.examen_item_parent)
                .build());
        this.examenChildItems = examenChildItems;
        this.examenParentItems = examenParentItems;
        this.activity = activity;

    }

    @Override
    public int getContentItemsTotal() {
        return examenChildItems.size(); // number of items of this section
    }
    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new ExamenChildHolder(view);
    }

    @Override
    public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ExamenChildHolder itemHolder = (ExamenChildHolder) holder;
        final ExamenChildItem childItem = examenChildItems.get(position);
        itemHolder.childTema.setText(childItem.getTema());
        itemHolder.childCalif.setText(childItem.getCalif());
        itemHolder.hijo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, InstrumentoEditarActivty.class);
                intent.putExtra("id", examenParentItems.getId());
                intent.putExtra("id2", examenParentItems.getId2());
                activity.startActivity(intent);
            }
        });
        if (examenChildItems.size()-1 == position){
            itemHolder.linea.setVisibility(View.VISIBLE);
        }else{
            itemHolder.linea.setVisibility(View.GONE);
        }

    }

    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
        return new ExamenParentHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
        ExamenParentHolder holderH = (ExamenParentHolder) holder;
        holderH.tema_titulo.setText("Aspecto");
        holderH.parcial.setText(examenParentItems.getParcial());
        holderH.fecha.setText(examenParentItems.getFecha());
        holderH.des.setText(examenParentItems.getDes());
        holderH.des.setVisibility(View.VISIBLE);
        holderH.padre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, InstrumentoEditarActivty.class);
                intent.putExtra("id", examenParentItems.getId());
                Log.e("efe",examenParentItems.getId());
                intent.putExtra("id2", examenParentItems.getId2());
                Log.e("efe2",examenParentItems.getId2());
                activity.startActivity(intent);
            }
        });
    }


}

