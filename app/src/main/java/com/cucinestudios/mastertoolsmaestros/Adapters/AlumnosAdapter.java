package com.cucinestudios.mastertoolsmaestros.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cucinestudios.mastertoolsmaestros.Models.AlumnosItem;
import com.cucinestudios.mastertoolsmaestros.R;

import java.util.ArrayList;

/**
 * Created by niniparra on 2/3/17.
 */
public class AlumnosAdapter extends RecyclerView.Adapter<AlumnosAdapter.ViewHolder> {
        ArrayList<AlumnosItem> items;
        Activity activity;

    public static class ViewHolder extends RecyclerView.ViewHolder {
    // each data item is just a string in this case
     public TextView nombre;
        public ViewHolder(View v) {
        super(v);
        nombre = (TextView) v.findViewById(R.id.alumnos_lista_nombre);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AlumnosAdapter(Activity activity, ArrayList<AlumnosItem> items) {
        this.items = items;
        this.activity = activity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AlumnosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        LayoutInflater inflater = (LayoutInflater) LayoutInflater.from(activity);
        View rowView = inflater.inflate(R.layout.alumnos_lista, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(rowView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        AlumnosItem ditem = items.get(position);
        holder.nombre.setText(ditem.getName());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }
}

