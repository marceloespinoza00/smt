package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Adapters.InstrumentoEditarAdapter;
import com.cucinestudios.mastertoolsmaestros.Interface.ModalEditarExamenInterface;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.ExamenChildItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;


/**
 * Created by jesusnieves on 4/4/17.
 */
public class InstrumentoEditarActivty extends AppCompatActivity implements ModalEditarExamenInterface {

    RecyclerView lista;
    RecyclerView.LayoutManager manager;
    Activity activity;
    Config config;
    Realm realm;
    public TrailingCircularDotsLoader loading;
    ArrayList<ExamenChildItem> examenChildItems;
    InstrumentoEditarAdapter examenItemAdapter;
    TextView fecha;
    TextView parcial;
    TextView tema;
    TextView des;
    FloatingActionButton agregar;
    FloatingActionButton guardar;
    String id;
    String id2;
    FloatingActionMenu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.examen_editar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Instrumento");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config= new Config();
        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        lista = (RecyclerView) findViewById(R.id.examen_editar_lista);
        fecha = (TextView) findViewById(R.id.examen_editar_fecha);
        parcial = (TextView) findViewById(R.id.examen_editar_parcial);
        tema = (TextView) findViewById(R.id.examen_editar_tema);
        des = (TextView) findViewById(R.id.examen_editar_des);
        agregar = (FloatingActionButton) findViewById(R.id.examen_editar_agregar);
        guardar = (FloatingActionButton) findViewById(R.id.examen_editar_guardar);
        menu = (FloatingActionMenu) findViewById(R.id.menu);
        lista.setHasFixedSize(true);
        manager = new LinearLayoutManager(activity);
        lista.setLayoutManager(manager);
        tema.setText("Aspecto");
        //fecha.setText("2017-03-07");
        des.setVisibility(View.VISIBLE);
        //des.setText("Linea de tiempo");
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
            id2 = bundle.getString("id2");
        }

        guardar.setVisibility(View.GONE);

        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu.close(true);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
//                DialogFragment newFragment = ModalEditarExamen.newInstance();
//                newFragment.show(getSupportFragmentManager(), "dialog");
//                Bundle args = new Bundle();
//                args.putString("id",id);
//                newFragment.setArguments(args);
//                return;
            }
        });


        lista.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && menu.getVisibility() == View.VISIBLE) {
                    menu.hideMenu(false);
                } else if (dy < 0 && menu.getVisibility() != View.VISIBLE) {
                    menu.showMenu(true);
                }
            }
        });


        getDetalle();
    }

    public void getDetalle(){
        examenChildItems = new ArrayList<>();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("portafolio",id);
        params.put("portafolioAlumno",id2);
        Log.e("ee",id);
        Log.e("ee2",id2+" "+user.getToken());
        client.setTimeout(300000);
        String url = config.getUrl()+config.getInstrumentosAlumno();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try{
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray instrumentos = data.getJSONArray("instrumentos");

                        Log.e("al",instrumentos.length()+"");

                        JSONArray temas = instrumentos.getJSONObject(0).getJSONArray("temas");

                        String titulo = instrumentos.getJSONObject(0).getString("nombre");
                        String descripcion = instrumentos.getJSONObject(0).getString("descripcion");
                        String fechaEntrega = instrumentos.getJSONObject(0).getString("fechaEntrega");
                        double promedio = round(instrumentos.getJSONObject(0).getDouble("promedio"),1);

                        String idAlumno = instrumentos.getJSONObject(0).getString("idAlumno");


                        DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                        DateTime temp = df.withOffsetParsed().parseDateTime(fechaEntrega);
                        DateTimeFormatter df2 = DateTimeFormat.forPattern("yyyy-MM-dd");

                        String nombreAspecto1 = temas.getJSONObject(0).getString("nombreAspecto1");
                        String nombreAspecto2 = temas.getJSONObject(0).getString("nombreAspecto2");
                        String nombreAspecto3 = temas.getJSONObject(0).getString("nombreAspecto3");
                        String nombreAspecto4 = temas.getJSONObject(0).getString("nombreAspecto4");
                        String nombreAspecto5 = temas.getJSONObject(0).getString("nombreAspecto5");

                        String aspecto1 = temas.getJSONObject(0).getString("Aspecto1");
                        String aspecto2 = temas.getJSONObject(0).getString("Aspecto2");
                        String aspecto3 = temas.getJSONObject(0).getString("Aspecto3");
                        String aspecto4 = temas.getJSONObject(0).getString("Aspecto4");
                        String aspecto5 = temas.getJSONObject(0).getString("Aspecto5");


                        if(!temas.getJSONObject(0).isNull("nombreAspecto1")){
                            examenChildItems.add(new ExamenChildItem("1",nombreAspecto1,aspecto1,false,"Aspecto1"));
                        }

                        if(!temas.getJSONObject(0).isNull("nombreAspecto2")){
                            examenChildItems.add(new ExamenChildItem("2",nombreAspecto2,aspecto2,false,"Aspecto2"));
                        }

                        if(!temas.getJSONObject(0).isNull("nombreAspecto3")){
                            examenChildItems.add(new ExamenChildItem("3",nombreAspecto3,aspecto3,false,"Aspecto3"));
                        }

                        if(!temas.getJSONObject(0).isNull("nombreAspecto4")){
                            examenChildItems.add(new ExamenChildItem("4",nombreAspecto4,aspecto4,false,"Aspecto4"));
                        }

                        if(!temas.getJSONObject(0).isNull("nombreAspecto5")){
                            examenChildItems.add(new ExamenChildItem("5",nombreAspecto5,aspecto5,false,"Aspecto5"));
                        }


                        examenChildItems.add(new ExamenChildItem("6","Total",promedio+"",true,""));

                        parcial.setText(titulo);
                        fecha.setText(temp.toString(df2));
                        des.setText(descripcion);

                        examenItemAdapter = new InstrumentoEditarAdapter(activity,id,idAlumno,examenChildItems);
                        lista.setAdapter(examenItemAdapter);

                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("einstrumento",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.alumnos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFinishEditDialog(String tema, String clafi) {
//        examenChildItems.add(examenChildItems.size()-1,new ExamenChildItem(1,tema,clafi,false));
//        examenChildItems.get(examenChildItems.size()-1).setCalif(examenItemAdapter.promedio());
//        examenItemAdapter.notifyDataSetChanged();
    }
}



