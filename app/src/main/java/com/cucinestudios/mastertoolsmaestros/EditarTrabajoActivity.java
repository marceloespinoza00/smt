package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Calendar;
import java.util.TimeZone;

import ru.slybeaver.slycalendarview.SlyCalendarDialog;

/**
 * Created by jesusnieves on 26/5/17.
 */
public class EditarTrabajoActivity extends AppCompatActivity implements SlyCalendarDialog.Callback{

    TextView dia;
    Activity activity;
    Button listo;
    String date;
    EditText nombre, tipo;
    TextInputLayout lnombre, ltipo;
    private static final String FRAG_TAG_DATE_PICKER = "fragment_date_picker_name";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nuevo_trabajo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Nuevo Trabajo");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        dia = (TextView) findViewById(R.id.nuevo_trabajo_dia);
        nombre = (EditText) findViewById(R.id.nuevo_trabajo_nombre);
        tipo = (EditText) findViewById(R.id.nuevo_trabajo_tipo);
        lnombre = (TextInputLayout) findViewById(R.id.nuevo_trabajo_layout_nombre);
        ltipo = (TextInputLayout) findViewById(R.id.nuevo_trabajo_layout_tipo);
        listo = (Button) findViewById(R.id.nuevo_trabajo_listo);
        listo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
        dia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SlyCalendarDialog()
                        .setSingle(true)
                        .setBackgroundColor(ContextCompat.getColor(activity, R.color.blanco))
                        .setHeaderTextColor(ContextCompat.getColor(activity, R.color.blanco))
                        .setHeaderColor(ContextCompat.getColor(activity, R.color.colorPrimary))
                        .setSelectedTextColor(ContextCompat.getColor(activity, R.color.blanco))
                        .setTextColor(ContextCompat.getColor(activity, R.color.negro))
                        .setCallback(EditarTrabajoActivity.this)
                        .show(getSupportFragmentManager(), FRAG_TAG_DATE_PICKER);
            }
        });
        getDetalle();
    }

    public void getDetalle(){
        nombre.setText("Trabajo uno");
        tipo.setText("Tipo1");
        date = 2017+"/"+(03+1)+"/"+20;
        dia.setText("20/04/2017");
    }

    private void submitForm() {
        if (!validateNombre()) {
            return;
        }
        if (!validateTipo()) {
            return;
        }
        if (!validateFecha()) {
            return;
        }
        finish();
    }

    private boolean validateNombre() {
        if (nombre.getText().toString().trim().isEmpty()) {
            lnombre.setError("No puede estar vacio");
            requestFocus(nombre);
            return false;
        } else {
            lnombre.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateTipo() {
        if (tipo.getText().toString().trim().isEmpty()) {
            ltipo.setError("No puede estar vacio");
            requestFocus(tipo);
            return false;
        } else {
            ltipo.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateFecha() {
        if (date.trim().isEmpty()) {
            dia.setError("No puede estar vacio");
            return false;
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.alumnos, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCancelled() {

    }

    @Override
    public void onDataSelected(Calendar firstDate, Calendar secondDate, int hours, int minutes) {
        if (firstDate != null) {
            TimeZone tz = firstDate.getTimeZone();
            DateTimeZone jodaTz = DateTimeZone.forID(tz.getID());
            DateTime dateTime = new DateTime(firstDate.getTimeInMillis(), jodaTz);
            dia.setText(dateTime.getDayOfMonth() + "/" + (dateTime.getMonthOfYear() + 1) + "/" + dateTime.getYear());
            date = dateTime.getYear() + "/" + (dateTime.getMonthOfYear() + 1) + "/" + dateTime.getDayOfMonth();
            Log.e("e",date);
        }
    }
}


