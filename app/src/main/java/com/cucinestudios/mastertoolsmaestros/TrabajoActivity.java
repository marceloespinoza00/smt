package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Adapters.TrabajoAdapter;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.TrabajoItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tapadoo.alerter.Alerter;
import com.tapadoo.alerter.OnHideAlertListener;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cz.msebera.android.httpclient.Header;
import io.github.codefalling.recyclerviewswipedismiss.SwipeDismissRecyclerViewTouchListener;
import io.realm.Realm;

//import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

/**
 * Created by jesusnieves on 20/3/17.
 */
public class TrabajoActivity extends AppCompatActivity {

    RecyclerView lista;
    RecyclerView.LayoutManager manager;
    Activity activity;
    Config config;
    Realm realm;
    TrailingCircularDotsLoader loading;
    ArrayList<TrabajoItem> trabajoItems;
    TrabajoAdapter trabajoAdapter;
    SwipeDismissRecyclerViewTouchListener listener;
    CoordinatorLayout coordinatorLayout;
    TrabajoItem item;
    FloatingActionButton crear;
    String id;
    String idBimestre;
    String idGrupo;
    FrameLayout play;
    FloatingActionMenu menu;
    String url = "https://www.youtube.com/watch?v=vmM8xetfdGI";
    boolean first = false;
    TextView semTrabajo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trabajo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        play = (FrameLayout) toolbar.findViewById(R.id.trabajo_play);
        semTrabajo = findViewById(R.id.semaforoTrabajo);
        toolbar.setTitle("Trabajos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config= new Config();
        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        lista = (RecyclerView) findViewById(R.id.trabajo_lista);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator);
        crear = (FloatingActionButton) findViewById(R.id.trabajo_agregar);
        menu = (FloatingActionMenu) findViewById(R.id.menu);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
            idBimestre = bundle.getString("idBimestre");
            idGrupo = bundle.getString("idGrupo");
        }

        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu.close(true);
                Intent intent = new Intent(activity,NuevoTrabajoActivity.class);
                intent.putExtra("idBimestre",idBimestre);
                intent.putExtra("idGrupo",idGrupo);
                startActivity(intent);
            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,VideoActivity.class);
                intent.putExtra("url",url);
                startActivity(intent);
            }
        });


        lista.setHasFixedSize(true);
        manager = new LinearLayoutManager(activity);
        lista.setLayoutManager(manager);
        listener = new SwipeDismissRecyclerViewTouchListener.Builder(
                lista,
                new SwipeDismissRecyclerViewTouchListener.DismissCallbacks() {
                    @Override
                    public boolean canDismiss(final int position) {
                        return true;
                    }

                    @Override
                    public void onDismiss(View view) {
                        final int position = lista.getChildPosition(view);
                        trabajoItems.remove(position);
                        trabajoAdapter.notifyItemRemoved(position);
                        Alerter.create(activity)
                                .setBackgroundColor(R.color.rojo)
                                .setText("Trabajo Eliminado")
                                .setIcon(R.drawable.bin)
                                .enableIconPulse(true)
                                .setDuration(500)
                                .setOnHideListener(new OnHideAlertListener() {
                                    @Override
                                    public void onHide() {
                                        trabajoAdapter.notifyDataSetChanged();
                                    }
                                })
                                .show();

                        // Do what you want when dismiss

                    }
                })
                .setIsVertical(false)
                .setItemClickCallback(new SwipeDismissRecyclerViewTouchListener.OnItemClickCallBack() {
                    @Override
                    public void onClick(int i) {

                    }
                })
                .setItemTouchCallback(
                        new SwipeDismissRecyclerViewTouchListener.OnItemTouchCallBack() {
                            @Override
                            public void onTouch(int index) {

                            }
                        })
                .create();
        //lista.setOnTouchListener(listener);
        getLista();
    }

    public void getLista(){
        trabajoItems = new ArrayList<>();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        String url = config.getUrl()+config.getTrabajosAlumnos();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        Log.i("gettoken","gettoken"+user.getToken()+" "+id+" "+idBimestre);
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
                first = true;
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    double entregados = 0;
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1) {
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray trabajos = data.getJSONArray("trabajos");
                        Log.e("al",trabajos.length()+"");
                        for (int i = 0; i<trabajos.length(); i++){
                            String name = trabajos.getJSONObject(i).getString("Nombre");
                            String tipo = "";
                            String fecha = trabajos.getJSONObject(i).getString("Fecha");
                            int estado = trabajos.getJSONObject(i).getInt("Estado");
                            if(estado==1){
                                entregados++;
                            }
                            DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                            DateTime temp = df.withOffsetParsed().parseDateTime(fecha);
                            DateTimeFormatter df2 = DateTimeFormat.forPattern("yyyy-MM-dd");

                            if(!trabajos.getJSONObject(i).isNull("Tipo")) {
                                tipo = trabajos.getJSONObject(i).getString("Tipo");
                            }

                            trabajoItems.add(new TrabajoItem(trabajos.getJSONObject(i).getString("IDTrabajo"),name,tipo,temp.toString(df2),estado));
                        }
                        double total = trabajos.length();
                        double promTrabajos = (entregados/total)*100;
                        int prom = (int) promTrabajos;
                        if(prom<=60){
                            semTrabajo.setBackgroundResource(R.drawable.semaforo_rojo);
                        }else if(promTrabajos<90){
                            semTrabajo.setBackgroundResource(R.drawable.semaforo_amarillo);
                        }else{
                            semTrabajo.setBackgroundResource(R.drawable.semaforo_verde);
                        }
                        Collections.sort(trabajoItems, new Comparator<TrabajoItem>() {
                            public int compare(TrabajoItem o1, TrabajoItem o2) {
                                return o2.getFecha().compareTo(o1.getFecha());
                            }
                        });
                        Log.e("al",trabajoItems.size()+"");
                        trabajoAdapter = new TrabajoAdapter(trabajoItems,activity);
                        lista.setAdapter(trabajoAdapter);

                    }else {
                        first = true;

                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                first = true;

                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });

    }
    public void getListaRefresh(){
        trabajoItems.clear();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        String url = config.getUrl()+config.getTrabajosAlumnos();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    double entregados = 0;
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1) {
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray trabajos = data.getJSONArray("trabajos");
                        Log.e("al",trabajos.length()+"");
                        for (int i = 0; i<trabajos.length(); i++){
                            String name = trabajos.getJSONObject(i).getString("Nombre");
                            String tipo = "";
                            String fecha = trabajos.getJSONObject(i).getString("Fecha");
                            int estado = trabajos.getJSONObject(i).getInt("Estado");

                            DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                            DateTime temp = df.withOffsetParsed().parseDateTime(fecha);
                            DateTimeFormatter df2 = DateTimeFormat.forPattern("yyyy-MM-dd");

                            if(!trabajos.getJSONObject(i).isNull("Tipo")) {
                                tipo = trabajos.getJSONObject(i).getString("Tipo");
                            }

                            trabajoItems.add(new TrabajoItem(trabajos.getJSONObject(i).getString("IDTrabajo"),name,tipo,temp.toString(df2),estado));
                        }
                        double total = trabajos.length();
                        double promTrabajos = (entregados/total)*100;
                        int prom = (int) promTrabajos;
                        if(prom<=60){
                            semTrabajo.setBackgroundResource(R.drawable.semaforo_rojo);
                        }else if(promTrabajos<90){
                            semTrabajo.setBackgroundResource(R.drawable.semaforo_amarillo);
                        }else{
                            semTrabajo.setBackgroundResource(R.drawable.semaforo_verde);
                        }
                        trabajoAdapter.notifyDataSetChanged();

                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });

    }



    public void actualizarEstado(String idtrabajo, final int estado, final int pos){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("trabajo",idtrabajo);
        params.put("estado",estado);
        String url = config.getUrl()+config.getUpdateTrabajoAlumno();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.put(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1) {
                        trabajoItems.get(pos).setEstado(estado);
                        trabajoAdapter.notifyDataSetChanged();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.alumnos, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (trabajoAdapter != null && trabajoItems != null && first) {
            getListaRefresh();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

}

