package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Adapters.DatoAdapter;
import com.cucinestudios.mastertoolsmaestros.Adapters.ExamenSectionAdapter;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.DatoChildItem;
import com.cucinestudios.mastertoolsmaestros.Models.DatoParentItem;
import com.cucinestudios.mastertoolsmaestros.Models.ExamenParentItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.realm.Realm;

/**
 * Created by jesusnieves on 27/4/17.
 */
public class DatosActivity extends AppCompatActivity {
    RecyclerView lista;
    RecyclerView.LayoutManager manager;
    Activity activity;
    ArrayList<DatoChildItem> datoChildItems;
    SectionedRecyclerViewAdapter datoAdapter;
    Button datos;
    Config config;
    Realm realm;
    TrailingCircularDotsLoader loading;
    String id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.datos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Datos Bimestre");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config = new Config();
        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        lista = (RecyclerView) findViewById(R.id.datos_lista);
        datos = (Button) findViewById(R.id.datos_ver);
        lista.setHasFixedSize(true);
        manager = new LinearLayoutManager(activity);
        lista.setLayoutManager(manager);
        datoAdapter = new SectionedRecyclerViewAdapter();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
        }

        datos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity,VerDatosActivity.class);
                startActivity(intent);
            }
        });

        getLista();
    }

    public void getLista(){
        datoChildItems = new ArrayList<>();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        String url = config.getUrl()+config.getControlBimestre();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }


            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray instrumentos1 = data.getJSONArray("instrumentos1");
                        JSONArray instrumentos2 = data.getJSONArray("instrumentos2");
                        JSONArray instrumentos3 = data.getJSONArray("instrumentos3");
                        JSONArray instrumentos4 = data.getJSONArray("instrumentos4");
                        JSONArray instrumentos5 = data.getJSONArray("instrumentos5");



                        for (int i = 0; i<instrumentos1.length(); i++){

                            String nombre = instrumentos1.getJSONObject(i).getString("Nombre");
                            double valor = round(instrumentos1.getJSONObject(i).getInt("total"),1);

                            datoChildItems.add(new DatoChildItem(nombre,valor+"",false));

                        }

                        if(!data.isNull("instrumentos1") && datoChildItems.size() > 0) {
                            DatoParentItem parentItem = new DatoParentItem("Bimestre 1",datoChildItems);
                            datoAdapter.addSection(new DatoAdapter(activity,parentItem,datoChildItems));
                        }

                        datoChildItems = new ArrayList<>();


                        for (int i = 0; i<instrumentos2.length(); i++){

                            String nombre = instrumentos2.getJSONObject(i).getString("Nombre");
                            double valor = round(instrumentos2.getJSONObject(i).getInt("total"),1);

                            datoChildItems.add(new DatoChildItem(nombre,valor+"",false));

                        }

                        if(!data.isNull("instrumentos2") && datoChildItems.size() > 0) {
                            DatoParentItem parentItem = new DatoParentItem("Bimestre 2",datoChildItems);
                            datoAdapter.addSection(new DatoAdapter(activity,parentItem,datoChildItems));
                        }

                        datoChildItems = new ArrayList<>();


                        for (int i = 0; i<instrumentos3.length(); i++){

                            String nombre = instrumentos3.getJSONObject(i).getString("Nombre");
                            double valor = round(instrumentos3.getJSONObject(i).getInt("total"),1);

                            datoChildItems.add(new DatoChildItem(nombre,valor+"",false));

                        }

                        if(!data.isNull("instrumentos3") && datoChildItems.size() > 0) {
                            DatoParentItem parentItem = new DatoParentItem("Bimestre 3",datoChildItems);
                            datoAdapter.addSection(new DatoAdapter(activity,parentItem,datoChildItems));
                        }

                        datoChildItems = new ArrayList<>();


                        for (int i = 0; i<instrumentos4.length(); i++){

                            String nombre = instrumentos4.getJSONObject(i).getString("Nombre");
                            double valor = round(instrumentos4.getJSONObject(i).getInt("total"),1);

                            datoChildItems.add(new DatoChildItem(nombre,valor+"",false));

                        }

                        if(!data.isNull("instrumentos4") && datoChildItems.size() > 0) {
                            DatoParentItem parentItem = new DatoParentItem("Bimestre 4",datoChildItems);
                            datoAdapter.addSection(new DatoAdapter(activity,parentItem,datoChildItems));
                        }

                        datoChildItems = new ArrayList<>();


                        for (int i = 0; i<instrumentos5.length(); i++){

                            String nombre = instrumentos5.getJSONObject(i).getString("Nombre");
                            double valor = round(instrumentos5.getJSONObject(i).getInt("total"),1);

                            datoChildItems.add(new DatoChildItem(nombre,valor+"",false));

                        }

                        if(!data.isNull("instrumentos5") && datoChildItems.size() > 0) {
                            DatoParentItem parentItem = new DatoParentItem("Bimestre 5",datoChildItems);
                            datoAdapter.addSection(new DatoAdapter(activity,parentItem,datoChildItems));
                        }

                        datoChildItems = new ArrayList<>();
                        lista.setAdapter(datoAdapter);
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }


    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.alumnos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

}



