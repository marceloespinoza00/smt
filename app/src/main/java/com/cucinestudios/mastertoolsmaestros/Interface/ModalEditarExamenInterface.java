package com.cucinestudios.mastertoolsmaestros.Interface;

/**
 * Created by jesusnieves on 14/3/17.
 */
public interface ModalEditarExamenInterface {
    void onFinishEditDialog(String tema,String clafi);
}
