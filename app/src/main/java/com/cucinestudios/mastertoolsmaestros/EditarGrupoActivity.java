package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.GenericoItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

/**
 * Created by niniparra on 2/3/17.
 */
public class EditarGrupoActivity extends AppCompatActivity
{
    EditText materia, escuela,ciclo,grupo,registro;
    TextInputLayout lmateria, lescuela,lciclo,lgrupo,lregistro;
    MaterialBetterSpinner grado,turno;
    Config config;
    Activity activity;
    ArrayAdapter<GenericoItem> gradoAdapter;
    ArrayList<GenericoItem> gradoItems;
    ArrayAdapter<GenericoItem> turnoAdapter;
    ArrayList<GenericoItem> turnoItems;
    Button clonar,archivar,eliminar;
    TextView blanco,verde,rojo,azulo,azul,naranja;
    String color = "";
    String grade,turn;
    FrameLayout play;
    TrailingCircularDotsLoader loading;
    String idGrupo = "";
    int estado;
    Realm realm;
    boolean isArchivar = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.configuracion);
        activity = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        play = (FrameLayout) toolbar.findViewById(R.id.configuracion_play);
        toolbar.setTitle("Grupos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        config = new Config();
        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        materia = (EditText) findViewById(R.id.configuracion_materia);
        escuela = (EditText) findViewById(R.id.configuracion_escuela);
        ciclo = (EditText) findViewById(R.id.configuracion_ciclo);
        grupo = (EditText) findViewById(R.id.configuracion_grupo);
        registro = (EditText) findViewById(R.id.configuracion_registro);
        lmateria = (TextInputLayout) findViewById(R.id.configuracion_layout_materia);
        lescuela = (TextInputLayout) findViewById(R.id.configuracion_layout_escuela);
        lciclo = (TextInputLayout) findViewById(R.id.configuracion_layout_ciclo);
        lgrupo = (TextInputLayout) findViewById(R.id.configuracion_layout_grupo);
        lregistro = (TextInputLayout) findViewById(R.id.configuracion_layout_registro);
        grado = (MaterialBetterSpinner) findViewById(R.id.configuracion_grado);
        turno = (MaterialBetterSpinner) findViewById(R.id.configuracion_turno);
        blanco = (TextView) findViewById(R.id.configuracion_blanco);
        rojo = (TextView) findViewById(R.id.configuracion_rojo);
        verde = (TextView) findViewById(R.id.configuracion_verde);
        azulo = (TextView) findViewById(R.id.configuracion_azulo);
        azul = (TextView) findViewById(R.id.configuracion_azul);
        naranja = (TextView) findViewById(R.id.configuracion_naranja);
        clonar = (Button) findViewById(R.id.configuracion_clonar);
        archivar = (Button) findViewById(R.id.configuracion_archivar);
        eliminar = (Button) findViewById(R.id.configuracion_eliminar);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            idGrupo = bundle.getString("idGrupo");
        }

        agregarGrado();
        agregarTurno();

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                estado = 0;
                statusGrupo();
            }
        });

        archivar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                estado = 2;
                if (isArchivar){
                    estado = 1;
                }
                statusGrupo();
            }
        });

        clonar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                clonarGrupo();
            }
        });

        rojo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rojo.setSelected(true);
                blanco.setSelected(false);
                verde.setSelected(false);
                azulo.setSelected(false);
                azul.setSelected(false);
                naranja.setSelected(false);
                color = String.format("#%06X", (0xFFFFFF & ContextCompat.getColor(activity,R.color.rojo)));
            }
        });

        azulo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rojo.setSelected(false);
                blanco.setSelected(false);
                verde.setSelected(false);
                azulo.setSelected(true);
                azul.setSelected(false);
                naranja.setSelected(false);
                color = String.format("#%06X", (0xFFFFFF & ContextCompat.getColor(activity,R.color.azul_oscuro)));
            }
        });

        azul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rojo.setSelected(false);
                blanco.setSelected(false);
                verde.setSelected(false);
                azulo.setSelected(false);
                azul.setSelected(true);
                naranja.setSelected(false);
                color = String.format("#%06X", (0xFFFFFF & ContextCompat.getColor(activity,R.color.azul)));
            }
        });

        naranja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rojo.setSelected(false);
                blanco.setSelected(false);
                verde.setSelected(false);
                azulo.setSelected(false);
                azul.setSelected(false);
                naranja.setSelected(true);
                color = String.format("#%06X", (0xFFFFFF & ContextCompat.getColor(activity,R.color.naranja)));
            }
        });

        verde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rojo.setSelected(false);
                blanco.setSelected(false);
                verde.setSelected(true);
                azulo.setSelected(false);
                azul.setSelected(false);
                naranja.setSelected(false);
                color = String.format("#%06X", (0xFFFFFF & ContextCompat.getColor(activity,R.color.verde)));
            }
        });

        blanco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rojo.setSelected(false);
                blanco.setSelected(true);
                verde.setSelected(false);
                azulo.setSelected(false);
                azul.setSelected(false);
                naranja.setSelected(false);
                color = String.format("#%06X", (0xFFFFFF & ContextCompat.getColor(activity,R.color.gris_grupo)));
            }
        });

        getDetalle();
    }

    public void getDetalle(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("grupo",idGrupo);
        String url = config.getUrl()+config.getGrupo();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray group = data.getJSONArray("group");

                        for (int i = 0; i<group.length(); i++){
                            String materiaGroup = group.getJSONObject(i).getString("Materia");
                            String escuelaGroup = group.getJSONObject(i).getString("Escuela");
                            String grupoGroup = group.getJSONObject(i).getString("Grupo");
                            String turnoGroup = group.getJSONObject(i).getString("Turno");
                            String gradoGroup = group.getJSONObject(i).getString("Grado");
                            String registroGroup = group.getJSONObject(i).getString("RegistroFederalEscolar");
                            String cicloGroup = group.getJSONObject(i).getString("Ciclo");
                            if (group.getJSONObject(i).getInt("Status") == 2){
                                isArchivar = true;
                            }

                            materia.setText(materiaGroup);
                            escuela.setText(escuelaGroup);
                            grupo.setText(grupoGroup);
                            registro.setText(registroGroup);
                            ciclo.setText(cicloGroup);

                            int posgrado = getPosGrado(gradoGroup);

                            int posturno = getPosTurno(turnoGroup);

                            grado.setText(gradoAdapter.getItem(posgrado).getName());
                            agregarGrado();

                            turno.setText(turnoAdapter.getItem(posturno).getName());
                            agregarTurno();

                            rojo.setSelected(true);
                            if (isArchivar){
                                archivar.setText("Des-archivar");
                            }

                        }
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });

    }


    public void setGrupo(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("grupoId",idGrupo);
        params.put("materia",materia.getText().toString());
        params.put("status",1);
        params.put("grado",grade);
        params.put("grupo",grupo.getText().toString());
        params.put("escuela",escuela.getText().toString());
        params.put("color",color);
        params.put("turno",turn);
        params.put("registroFederal",registro.getText().toString());
        params.put("ciclo",ciclo.getText().toString());
        String url = config.getUrl()+config.getUpdateGrupo();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.put(url,params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                        finish();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }


    public void statusGrupo(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("grupoId",idGrupo);
        params.put("status",estado);
        String url = config.getUrl()+config.getStatusGrupo();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.put(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        if (estado == 0) {
                            Toast.makeText(activity,"El grupo ha sido eliminado",Toast.LENGTH_SHORT).show();
                        }else if (estado == 2) {
                            Toast.makeText(activity,"El grupo ha sido archivado",Toast.LENGTH_SHORT).show();
                        }else if (estado == 1) {
                            Toast.makeText(activity,"El grupo ha sido des-archivado",Toast.LENGTH_SHORT).show();
                        }

                        finish();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void clonarGrupo(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("grupo",idGrupo);
        String url = config.getUrl()+config.getClonarGrupo();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.post(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                        finish();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void submitForm() {
        if (!validateCiclo()) {
            return;
        }
        if (!validateColor()) {
            return;
        }
        if (!validateEscuela()) {
            return;
        }
        if (!validateGrupo()) {
            return;
        }
        if (!validateRegistro()) {
            return;
        }
        if (!validateMateria()) {
            return;
        }

        setGrupo();
    }


    private boolean validateEscuela() {
        if (escuela.getText().toString().trim().isEmpty()) {
            lescuela.setError("No puede estar vacio");
            requestFocus(escuela);
            return false;
        } else {
            lescuela.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateMateria() {
        if (materia.getText().toString().trim().isEmpty()) {
            lmateria.setError("No puede estar vacio");
            requestFocus(materia);
            return false;
        } else {
            lmateria.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateRegistro() {
        if (registro.getText().toString().trim().isEmpty()) {
            lregistro.setError("No puede estar vacio");
            requestFocus(registro);
            return false;
        } else {
            lregistro.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateCiclo() {
        if (ciclo.getText().toString().trim().isEmpty()) {
            lciclo.setError("No puede estar vacio");
            requestFocus(ciclo);
            return false;
        } else {
            lciclo.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateGrupo() {
        if (grupo.getText().toString().trim().isEmpty()) {
            lgrupo.setError("No puede estar vacio");
            requestFocus(grupo);
            return false;
        } else {
            lgrupo.setErrorEnabled(false);
        }

        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private boolean validateColor() {
        if (color.trim().isEmpty()) {
            Toast.makeText(activity,"Debes agregar un color",Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    public int getPosGrado(String codigo){
        int pos = 0;
        for (int i=0; i<gradoItems.size();i++){
            if (gradoItems.get(i).getName().matches(codigo)){
                pos = i;
            }
        }
        return pos;
    }


    public int getPosTurno(String codigo){
        int pos = 0;
        for (int i=0; i<turnoItems.size();i++){
            if (turnoItems.get(i).getName().matches(codigo)){
                pos = i;
            }
        }
        return pos;
    }


    public void agregarGrado(){
        gradoItems = new ArrayList<>();
        for (int i= 0; i<6;i++){
            gradoItems.add(new GenericoItem(i+1,""+(i+1)));
        }
        gradoAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_dropdown_item_1line, gradoItems);
        grado.setAdapter(gradoAdapter);
        grado.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                grade = gradoItems.get(position).getName();
            }
        });
    }


    public void agregarTurno(){
        turnoItems = new ArrayList<>();
        turnoItems.add(new GenericoItem(1,"Matutino"));
        turnoItems.add(new GenericoItem(2,"Vespertino"));
        turnoItems.add(new GenericoItem(3,"Nocturno"));
        turnoAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_dropdown_item_1line, turnoItems);
        turno.setAdapter(turnoAdapter);
        turno.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                turn = turnoItems.get(position).getName();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
