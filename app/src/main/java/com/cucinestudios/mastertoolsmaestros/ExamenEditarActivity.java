package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Adapters.ExamenEditarAdapter;
import com.cucinestudios.mastertoolsmaestros.Interface.ModalEditarExamenInterface;
import com.cucinestudios.mastertoolsmaestros.Modals.ModalEditarExamen;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.ExamenChildItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

/**
 * Created by jesusnieves on 13/3/17.
 */
public class ExamenEditarActivity extends AppCompatActivity implements ModalEditarExamenInterface {

    RecyclerView lista;
    RecyclerView.LayoutManager manager;
    Activity activity;
    ArrayList<ExamenChildItem> examenChildItems;
    ExamenEditarAdapter examenItemAdapter;
    TextView fecha;
    TextView parcial;
    TextView des;
    FloatingActionButton agregar;
    FloatingActionButton guardar;
    FloatingActionMenu menu;
    Config config;
    Realm realm;
    public TrailingCircularDotsLoader loading;
    String id;
    String idExamen;
    String idGrupo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.examen_editar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Examen");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config= new Config();
        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        lista = (RecyclerView) findViewById(R.id.examen_editar_lista);
        fecha = (TextView) findViewById(R.id.examen_editar_fecha);
        parcial = (TextView) findViewById(R.id.examen_editar_parcial);
        des = (TextView) findViewById(R.id.examen_editar_des);
        agregar = (FloatingActionButton) findViewById(R.id.examen_editar_agregar);
        guardar = (FloatingActionButton) findViewById(R.id.examen_editar_guardar);
        menu = (FloatingActionMenu) findViewById(R.id.menu);
        lista.setHasFixedSize(true);
        manager = new LinearLayoutManager(activity);
        lista.setLayoutManager(manager);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
            idExamen = bundle.getString("idExamen");
            idGrupo = bundle.getString("idGrupo");
        }

        guardar.setVisibility(View.GONE);

        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu.close(true);
                DialogFragment newFragment = ModalEditarExamen.newInstance();
                newFragment.show(getSupportFragmentManager(), "dialog");
                Bundle args = new Bundle();
                args.putString("id",id);
                args.putString("idExamen",idExamen);
                args.putString("idGrupo",idGrupo);
                newFragment.setArguments(args);
                return;

            }
        });


        lista.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && menu.getVisibility() == View.VISIBLE) {
                    menu.hideMenu(false);
                } else if (dy < 0 && menu.getVisibility() != View.VISIBLE) {
                    menu.showMenu(true);
                }
            }
        });


        getDetalle();
    }

    public void getDetalle(){
        examenChildItems = new ArrayList<>();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("examen",idExamen);
        Log.e("ee",id);
        String url = config.getUrl()+config.getExamen();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try{
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if (respuesta.getInt("status") == 1) {
                        String prome = "";
                        double prom = 0.0;
                        double sumpromedio = 0.0;
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray examenes = data.getJSONArray("examenes");
                        JSONArray temas = examenes.getJSONObject(0).getJSONArray("temas");

                        String titulo = examenes.getJSONObject(0).getString("titulo");
                        String fechaEntrega = examenes.getJSONObject(0).getString("fechaEntrega");
                        String descrip = examenes.getJSONObject(0).getString("tipo");

                        DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                        DateTime temp = df.withOffsetParsed().parseDateTime(fechaEntrega);
                        DateTimeFormatter df2 = DateTimeFormat.forPattern("yyyy-MM-dd");

                        parcial.setText(titulo);
                        fecha.setText(temp.toString(df2));
                        des.setText(descrip);

                        Log.e("al",examenes.length()+"");

                        for (int i = 0; i<temas.length(); i++){

                            String nombreTema = temas.getJSONObject(i).getString("Nombre");
                            int calificacion = temas.getJSONObject(i).getInt("Calificacion");
                            sumpromedio += calificacion;

                            examenChildItems.add(new ExamenChildItem(temas.getJSONObject(i).getString("IDTema"),nombreTema,calificacion+"",false,""));

                        }
                        prom = sumpromedio/examenChildItems.size();
                        double promedio = round(prom,1);
                        prome = String.valueOf(promedio);
                        examenChildItems.add(new ExamenChildItem(examenes.getJSONObject(0).getString("idExamen"),"Total",prome,true,""));


                        examenItemAdapter = new ExamenEditarAdapter(activity,id,examenChildItems);
                        lista.setAdapter(examenItemAdapter);

                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }


    public void getDetalleRefresh(){
        examenChildItems.clear();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("examen",idExamen);
        String url = config.getUrl()+config.getExamen();
        Log.e("url",url+params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try{
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if (respuesta.getInt("status") == 1) {
                        String prome = "";
                        double prom = 0.0;
                        double sumpromedio = 0.0;
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray examenes = data.getJSONArray("examenes");
                        JSONArray temas = examenes.getJSONObject(0).getJSONArray("temas");

                        String titulo = examenes.getJSONObject(0).getString("titulo");
                        String fechaEntrega = examenes.getJSONObject(0).getString("fechaEntrega");
                        String descrip = examenes.getJSONObject(0).getString("tipo");

                        DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                        DateTime temp = df.withOffsetParsed().parseDateTime(fechaEntrega);
                        DateTimeFormatter df2 = DateTimeFormat.forPattern("yyyy-MM-dd");

                        parcial.setText(titulo);
                        fecha.setText(temp.toString(df2));
                        des.setText(descrip);

                        Log.e("al",examenes.length()+"");

                        for (int i = 0; i<temas.length(); i++){

                            String nombreTema = temas.getJSONObject(i).getString("Nombre");
                            int calificacion = temas.getJSONObject(i).getInt("Calificacion");
                            sumpromedio += calificacion;

                            examenChildItems.add(new ExamenChildItem(temas.getJSONObject(i).getString("IDTema"),nombreTema,calificacion+"",false,""));

                        }
                        prom = sumpromedio/examenChildItems.size();
                        double promedio = round(prom,1);
                        prome = String.valueOf(promedio);
                        examenChildItems.add(new ExamenChildItem(examenes.getJSONObject(0).getString("idExamen"),"Total",prome,true,""));

                        examenItemAdapter.notifyDataSetChanged();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.alumnos, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onFinishEditDialog(String tema, String clafi) {
        if(examenItemAdapter != null){
            getDetalleRefresh();
        }
//        examenChildItems.add(examenChildItems.size()-1,new ExamenChildItem(examenChildItems.size(),tema,clafi,false));
//        examenChildItems.get(examenChildItems.size()-1).setCalif(examenItemAdapter.promedio());
//        examenItemAdapter.notifyDataSetChanged();

    }
}


