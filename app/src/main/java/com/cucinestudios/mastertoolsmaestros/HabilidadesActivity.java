package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.GenericoItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

/**
 * Created by jesusnieves on 5/4/17.
 */
public class HabilidadesActivity extends AppCompatActivity
{
    Config config;
    Activity activity;
    Spinner auto, coe, conocimiento,argu,sin,lectura,escritura,clases,mate;
    ArrayList<GenericoItem> autoItems,coeItems,conocimientoItems,arguItems,sinItems,lecturaItems,escrituraItems,clasesItems,mateItems;
    ArrayAdapter<GenericoItem> autoAdapter,coeAdapter,conocimientoAdapter,arguAdapter,sinAdapter,lecturaAdapter,escrituraAdapter,clasesAdapter,mateAdapter;
    Toolbar toolbar;
    Button guardar;
    String auto_select,coe_select,cono_select,argu_select,sin_select,lectura_select,escritura_select,clases_select,mate_select;
    Realm realm;
    TrailingCircularDotsLoader loading;
    FrameLayout play;
    String idBimestre;
    String idGrupo;
    String id;
    boolean hasUpdate = false;
    boolean isInvolucra;
    String url = "https://www.youtube.com/watch?v=BPXG8wJro-k";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.habilidades);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Habilidades");
        setSupportActionBar(toolbar);
        play = (FrameLayout) toolbar.findViewById(R.id.habilidades_play);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        config = new Config();
        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        auto = (Spinner) findViewById(R.id.habilidades_auto);
        coe = (Spinner) findViewById(R.id.habilidades_coe);
        conocimiento = (Spinner) findViewById(R.id.habilidades_conocimiento);
        argu = (Spinner) findViewById(R.id.habilidades_argu);
        sin = (Spinner) findViewById(R.id.habilidades_sin);
        lectura = (Spinner) findViewById(R.id.habilidades_lectura);
        escritura = (Spinner) findViewById(R.id.habilidades_escritura);
        clases = (Spinner) findViewById(R.id.habilidades_clases);
        mate = (Spinner) findViewById(R.id.habilidades_mate);
        guardar = (Button) findViewById(R.id.habilidades_guardar);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
            idBimestre = bundle.getString("idBimestre");
            idGrupo = bundle.getString("idGrupo");
        }

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hasUpdate){
                    updateLista();
                }else {
                    setLista();
                }
            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,VideoActivity.class);
                intent.putExtra("url",url);
                startActivity(intent);
            }
        });

        agregarListas();


    }

    public void setLista(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        params.put("grupo",idGrupo);
        params.put("coevaluacion",coe_select);
        params.put("autoevaluacion",auto_select);
        params.put("comprension",cono_select);
        params.put("conocimiento",cono_select);
        params.put("sintesis",sin_select);
        params.put("argumentacion",argu_select);
        params.put("apoyoLectura",lectura_select);
        params.put("apoyoEscritura",escritura_select);
        params.put("apoyoMatematicas",mate_select);
        params.put("seInvolucraClase",isInvolucra);
        String url = config.getUrl()+config.getCreateHabilidadesAlumno();
        Log.e("url",params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.post(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

        });

    }

    public void updateLista(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        params.put("grupo",idGrupo);
        params.put("coevaluacion",coe_select);
        params.put("autoevaluacion",auto_select);
        params.put("comprension",cono_select);
        params.put("conocimiento",cono_select);
        params.put("sintesis",sin_select);
        params.put("argumentacion",argu_select);
        params.put("apoyoLectura",lectura_select);
        params.put("apoyoEscritura",escritura_select);
        params.put("apoyoMatematicas",mate_select);
        params.put("seInvolucraClase",isInvolucra);
        String url = config.getUrl()+config.getUpdateHabilidadesAlumno();
        Log.e("url",params.toString());
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.put(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

        });

    }

    public void getLista(){
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",idBimestre);
        params.put("grupo",idGrupo);
        client.setTimeout(300000);
        String url = config.getUrl()+config.getHabilidadesAlumno();
        Log.e("url",url);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        if (respuesta.has("data")) {
                            hasUpdate = true;
                            JSONObject data = respuesta.getJSONObject("data");
                            JSONArray habilidad = data.getJSONArray("habilidad");

                            int autoevaluacion = habilidad.getJSONObject(0).getInt("Autoevaluacion");
                            int coevaluacion = habilidad.getJSONObject(0).getInt("Coevaluacion");

                            String conocimientoHab = habilidad.getJSONObject(0).getString("Conocimiento");
                            String sintesis = habilidad.getJSONObject(0).getString("Sintesis");
                            String argumentacion = habilidad.getJSONObject(0).getString("Argumentacion");

                            boolean involucra = habilidad.getJSONObject(0).getBoolean("SeInvolucraClase");

                            int apoyoLectura = habilidad.getJSONObject(0).getInt("ApoyoLectura");
                            int apoyoEscritura = habilidad.getJSONObject(0).getInt("ApoyoEscritura");
                            int apoyoMatematicas = habilidad.getJSONObject(0).getInt("ApoyoMatematicas");

                            int posc = getPosCono(conocimientoHab);
                            conocimiento.setSelection(posc);

                            int poss = getPosSin(sintesis);
                            sin.setSelection(poss);

                            int posa = getPosArgu(argumentacion);
                            argu.setSelection(posa);

                            auto.setSelection(getPosAuto(autoevaluacion));
                            coe.setSelection(getPosCoe(coevaluacion));

                            lectura.setSelection(getPosLec(apoyoLectura));
                            escritura.setSelection(getPosEsc(apoyoEscritura));
                            mate.setSelection(getPosMate(apoyoMatematicas));
                            isInvolucra = involucra;
                            if (involucra) {
                                clases.setSelection(1);
                            } else {
                                clases.setSelection(2);
                            }
                        }else{
                            hasUpdate = false;
                        }
                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

        });

    }

    public void agregarListas(){
        autoItems = new ArrayList<>();
        coeItems = new ArrayList<>();
        lecturaItems = new ArrayList<>();
        escrituraItems = new ArrayList<>();
        mateItems = new ArrayList<>();
        clasesItems = new ArrayList<>();
        conocimientoItems = new ArrayList<>();
        arguItems = new ArrayList<>();
        sinItems = new ArrayList<>();

        for (int i=4; i<10; i++){
            autoItems.add(new GenericoItem(i+1,(i+1)+""));
            coeItems.add(new GenericoItem(i+1,(i+1)+""));
            lecturaItems.add(new GenericoItem(i+1,(i+1)+""));
            escrituraItems.add(new GenericoItem(i+1,(i+1)+""));
            mateItems.add(new GenericoItem(i+1,(i+1)+""));
        }

        clasesItems.add(new GenericoItem(0,"Seleccione"));
        clasesItems.add(new GenericoItem(1,"Si"));
        clasesItems.add(new GenericoItem(2,"No"));

        conocimientoItems.add(new GenericoItem(0,"Seleccione"));
        conocimientoItems.add(new GenericoItem(1,"Siempre"));
        conocimientoItems.add(new GenericoItem(2,"Casi siempre"));
        conocimientoItems.add(new GenericoItem(3,"En ocasiones"));
        conocimientoItems.add(new GenericoItem(4,"Requiere Apoyo"));



        arguItems.add(new GenericoItem(0,"Seleccione"));
        arguItems.add(new GenericoItem(1,"Siempre"));
        arguItems.add(new GenericoItem(2,"Casi siempre"));
        arguItems.add(new GenericoItem(3,"En ocasiones"));
        arguItems.add(new GenericoItem(4,"Requiere Apoyo"));

        sinItems.add(new GenericoItem(0,"Seleccione"));
        sinItems.add(new GenericoItem(1,"Siempre"));
        sinItems.add(new GenericoItem(2,"Casi siempre"));
        sinItems.add(new GenericoItem(3,"En ocasiones"));
        sinItems.add(new GenericoItem(4,"Requiere Apoyo"));

        autoAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_dropdown_item_1line, autoItems);
        auto.setAdapter(autoAdapter);
        auto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                auto_select = autoItems.get(i).getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        coeAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_dropdown_item_1line, coeItems);
        coe.setAdapter(coeAdapter);
        coe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                coe_select = coeItems.get(i).getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        conocimientoAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_dropdown_item_1line, conocimientoItems);
        conocimiento.setAdapter(conocimientoAdapter);
        conocimiento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cono_select = conocimientoItems.get(i).getName().substring(0,1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        arguAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_dropdown_item_1line, arguItems);
        argu.setAdapter(arguAdapter);
        argu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                argu_select = arguItems.get(i).getName().substring(0,1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sinAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_dropdown_item_1line, sinItems);
        sin.setAdapter(sinAdapter);
        sin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sin_select = sinItems.get(i).getName().substring(0,1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        lecturaAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_dropdown_item_1line, lecturaItems);
        lectura.setAdapter(lecturaAdapter);
        lectura.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                lectura_select = lecturaItems.get(i).getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        escrituraAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_dropdown_item_1line, escrituraItems);
        escritura.setAdapter(escrituraAdapter);
        escritura.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                escritura_select = escrituraItems.get(i).getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        clasesAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_dropdown_item_1line, clasesItems);
        clases.setAdapter(clasesAdapter);
        clases.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                clases_select = clasesItems.get(i).getName();
                if (clases_select.toLowerCase().equals("si")){
                    isInvolucra = true;
                }else{
                    isInvolucra = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mateAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_dropdown_item_1line, mateItems);
        mate.setAdapter(mateAdapter);
        mate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mate_select = mateItems.get(i).getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        getLista();
    }

    public int getPosAuto(int auto){
        int pos = 0;
        for (int i=0; i<autoItems.size();i++){
            if (autoItems.get(i).getId() == auto){
                pos = i;
            }
        }
        return pos;
    }
    public int getPosCoe(int coe){
        int pos = 0;
        for (int i=0; i<coeItems.size();i++){
            if (coeItems.get(i).getId() == coe){
                pos = i;
            }
        }
        return pos;
    }
    public int getPosEsc(int esc){
        int pos = 0;
        for (int i=0; i<escrituraItems.size();i++){
            if (escrituraItems.get(i).getId() == esc){
                pos = i;
            }
        }
        return pos;
    }
    public int getPosLec(int lec){
        int pos = 0;
        for (int i=0; i<lecturaItems.size();i++){
            if (lecturaItems.get(i).getId() == lec){
                pos = i;
            }
        }
        return pos;
    }
    public int getPosMate(int mate){
        int pos = 0;
        for (int i=0; i<mateItems.size();i++){
            if (mateItems.get(i).getId() == mate){
                pos = i;
            }
        }
        return pos;
    }
    public int getPosCono(String cono){
        int pos = 0;
        for (int i=0; i<conocimientoItems.size();i++){
            if (conocimientoItems.get(i).getName().startsWith(cono)){
                pos = i;
            }
        }
        return pos;
    }

    public int getPosSin(String sintesis){
        int pos = 0;
        for (int i=0; i<sinItems.size();i++){
            if (sinItems.get(i).getName().startsWith(sintesis)){
                pos = i;
            }
        }
        return pos;
    }

    public int getPosArgu(String argumento){
        int pos = 0;
        for (int i=0; i<arguItems.size();i++){
            if (arguItems.get(i).getName().startsWith(argumento)){
                pos = i;
            }
        }
        return pos;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.alumno, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

