package com.cucinestudios.mastertoolsmaestros;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;
import com.cucinestudios.mastertoolsmaestros.Adapters.RendimientoAdapter;
import com.cucinestudios.mastertoolsmaestros.Models.Config;
import com.cucinestudios.mastertoolsmaestros.Models.RendimientoItem;
import com.cucinestudios.mastertoolsmaestros.Models.User;
import com.github.mikephil.charting.charts.BubbleChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BubbleData;
import com.github.mikephil.charting.data.BubbleDataSet;
import com.github.mikephil.charting.data.BubbleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.IBubbleDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

/**
 * Created by niniparra on 26/4/17.
 */
public class RendimientoActivity extends AppCompatActivity {

    RecyclerView lista;
    GridLayoutManager manager;
    Activity activity;
    private LineChart chart;
    ArrayList<RendimientoItem> rendimientoItems;
    RendimientoAdapter rendimientoAdapter;
    Config config;
    Realm realm;
    TrailingCircularDotsLoader loading;
    String id;
    String grupo;
    int bimestre;
    List<Integer> colors;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rendimiento);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Rendimiento");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        chart = findViewById(R.id.chart1);
        activity = this;
        config = new Config();
        realm = Realm.getDefaultInstance();
        loading = (TrailingCircularDotsLoader) findViewById(R.id.loading);
        lista = (RecyclerView) findViewById(R.id.rendimiento_lista);
        lista.setHasFixedSize(true);
        manager = new GridLayoutManager(activity,3);
        lista.setLayoutManager(manager);
        chart.setVisibility(View.INVISIBLE);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id = bundle.getString("id");
            bimestre = bundle.getInt("bimestre");
            grupo = bundle.getString("grupo");
        }
        chart.setTouchEnabled(true);

        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setDrawGridBackground(false);
        chart.getDescription().setEnabled(false);
        chart.setDrawBorders(false);
        /*setAxis();*/
        getLista();
    }


 /*   public void setAxis(){
        chartView.getDescription().setEnabled(false);
        chartView.setPinchZoom(false);
        YAxis yl = chartView.getAxisLeft();
        yl.setDrawZeroLine(false);
        yl.setTextColor(ContextCompat.getColor(activity,R.color.negro));
        chartView.getAxisRight().setEnabled(false);
        XAxis xl = chartView.getXAxis();
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setTextColor(ContextCompat.getColor(activity,R.color.negro));
        getLista();
    }*/


    public void getLista(){
        //In most cased you can call data model methods in builder-pattern-like manner.
        rendimientoItems = new ArrayList<>();
        User user = realm.where(User.class).findFirst();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("alumno",id);
        params.put("bimestre",bimestre);
        params.put("grupo",grupo);
        String url = config.getUrl()+config.getListControl();
        Log.e("url",url);
        client.setTimeout(120000);
        client.addHeader("Authorization", "Bearer "+user.getToken());
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                loading.setVisibility(View.VISIBLE);
            }


            @Override
            public void onFinish() {
                super.onFinish();
                loading.setVisibility(View.GONE);
            }


            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    JSONObject respuesta = new JSONObject(new String(response));
                    Log.e("res",respuesta.toString());
                    if(respuesta.getInt("status") == 1){
                        /*---Linechart----*/
                        ArrayList<Entry> values2 = new ArrayList<>();
                        ArrayList<ILineDataSet> dataSets3 = new ArrayList<>();
                        /*-----Linechart----*/
                        JSONObject data = respuesta.getJSONObject("data");
                        JSONArray instrumentos = data.getJSONArray("instrumentos");
                        List<BubbleEntry> values = new ArrayList<>();
                        colors = new ArrayList<>();
                        for (int e = 0; e<instrumentos.length(); e++) {
                            String nombre = instrumentos.getJSONObject(e).getString("Nombre");
                            double i1 = instrumentos.getJSONObject(e).getDouble("total");
                            Random rnd = new Random();
                            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                            colors.add(color);
                            BubbleEntry value = new BubbleEntry(e, (float) i1,(float) i1);
                            values.add(value);
                            rendimientoItems.add(new RendimientoItem(nombre,color));
                            values2.add(new Entry(e, (float) i1));
                        }
                        LineDataSet d = new LineDataSet(values2, "Calificacion");
                        d.setColors(colors);
                        dataSets3.add(d);
                        LineData data2 = new LineData(dataSets3);
                        chart.setData(data2);
                        chart.invalidate();
                        chart.setVisibility(View.VISIBLE);
                        rendimientoAdapter = new RendimientoAdapter(activity, rendimientoItems);
                        rendimientoAdapter.setHasStableIds(true);
                        lista.setAdapter(rendimientoAdapter);


                    }else {
                        Toast.makeText(activity,respuesta.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    Log.e("e",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable e) {
                Log.e("error",e.getMessage(),e);
                Toast.makeText(activity,"Ocurrio un error en el servidor. Por favor trate nuevamente",Toast.LENGTH_SHORT).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });


//        for (int i=0;i<1;i++){
//            List<PointValue> values = new ArrayList<PointValue>();
//
//            for (int j = 0; j < 5; ++j) {
//                Random r = new Random();
//                int i1 = r.nextInt(20);
//                Log.e("j",i1+"");
//                values.add(new PointValue(j, i1));
//
//            }
//            Line line = new Line(values);
//            line.setColor(ContextCompat.getColor(activity,R.color.colorPrimary));
//            line.setStrokeWidth(1);  f
//            line.setHasLabels(true);
//            line.setHasLines(true);
//            lines.add(line);
//        }

//        LineChartData data = new LineChartData(lines);
//        data.setAxisXBottom(axisX);
//        data.setAxisYLeft(axisY);
//        data.setBaseValue(0);
//        chartView.setLineChartData(data);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.alumnos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                super.onBackPressed();

                break;
            case R.id.action_play:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}